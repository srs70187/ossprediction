# Copyright (c) 2013 Georgios Gousios
# MIT-licensed
SET @@global.sql_mode= 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

drop table if exists Votes;
CREATE TABLE Votes (
    Id INT NOT NULL PRIMARY KEY,
    PostId INT NOT NULL,
    VoteTypeId SMALLINT NOT NULL,
    UserId INT,
    CreationDate DATETIME,
    BountyAmount INT
);
load xml infile '/var/lib/mysql-files/Votes.xml'
into table Votes
rows identified by '<row>';
create index Votes_idx_1 on Votes(PostId);

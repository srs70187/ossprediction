# Copyright (c) 2013 Georgios Gousios
# MIT-licensed
SET @@global.sql_mode= 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

drop table if exists Tags;
CREATE TABLE Tags (
  Id INT NOT NULL PRIMARY KEY,
  TagName VARCHAR(50) DEFAULT NULL,
  Count INT DEFAULT NULL,
  ExcerptPostId INT DEFAULT NULL,
  WikiPostId INT DEFAULT NULL
);
load xml infile '/var/lib/mysql-files/Tags.xml'
INTO TABLE Tags
ROWS IDENTIFIED BY '<row>';

# Copyright (c) 2013 Georgios Gousios
# MIT-licensed
SET @@global.sql_mode= 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

drop table if exists Badges;
create table Badges (
  Id INT NOT NULL PRIMARY KEY,
  UserId INT,
  Name VARCHAR(50),
  Date DATETIME,
  Class SMALLINT,
  TagBased VARCHAR(20)
);
load xml infile '/var/lib/mysql-files/Badges.xml'
into table Badges
rows identified by '<row>';

create index Badges_idx_1 on Badges(UserId);

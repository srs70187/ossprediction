#!/bin/bash

source_path=/raw-data-disk/xml
dest_path=~/msft-oss-eng/services/databases/ghtorrent/data/mysql-2018-03-01
cd $source_path
for f in *.xml; do
    echo $f
    head -500 $f > $dest_path/$f
done


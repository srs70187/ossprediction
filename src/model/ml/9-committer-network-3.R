library(magrittr)
library(dplyr)
library(dbplyr)
library(zoo)
library(tidyr)
library(stringr)
library(igraph)
library(parallel)
library(doParallel)
library(testthat)
library(future)
library(future.apply)
library(lubridate)
library(purrr)
library(tictoc)
library(readr)
plan(multiprocess,gc=T)

min_date <- as.Date("2007-10-01")
max_date <- read_lines("/datadrive/effective_stack_data_date") %>% as.Date() + months(1)

source_dir <- "/datadrive/peter"
target_dir <- "/datadrive/peter"


all_dates <- seq.Date(from=min_date, to=max_date, by="month")

author_cn <- lapply(1:length(all_dates), function(xx) {
  res <- readRDS(paste0("/datadrive/peter/graph/results_",all_dates[[xx]],".rds"))
  a_df <- do.call(cbind.data.frame, res[1:4])
  a_df$user_id <- names(res$a_degree)
  a_df$date <- format(all_dates[xx])
  return(a_df)
})
author_cn <- bind_rows(author_cn)

saveRDS(author_cn, file.path("/datadrive/peter", "author_cn.rds"))

committer_cn <- lapply(1:length(all_dates), function(xx) {
  res <- readRDS(paste0("/datadrive/peter/graph/results_",all_dates[[xx]],".rds"))
  c_df <- do.call(cbind.data.frame, res[5:8])
  c_df$user_id <- names(res$c_degree)
  c_df$date <- format(all_dates[xx])
  return(c_df)
})
committer_cn <- bind_rows(committer_cn)

saveRDS(committer_cn, file.path("/datadrive/peter", "committer_cn.rds"))

network_metrics <- author_cn %>% full_join(committer_cn, by = c("user_id", "date"))
network_metrics %<>% select(user_id, date, a_degree, c_degree, a_eigen, c_eigen, a_hub, c_hub, a_local_transitivity, c_local_transitivity) %>%
  arrange(user_id)

expect_gte(nrow(network_metrics), 3.481e6, label="Number of rows of network_metrics data frame")
saveRDS(network_metrics, file=file.path(target_dir, "network_metrics.rds"))

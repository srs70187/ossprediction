library(dplyr)
library(magrittr)
library(caret)
library(glmnet)
library(stringr)
library(imputeTS)
library(tidyr)
library(randomForest)
library(doMC)
library(xtable)
library(reporttools)
library(ranger)
library(lubridate)
library(zoo)
library(pROC)
library(readr)

source_dir <- "/datadrive/peter"
target_dir <- "/datadrive/peter"

max_date <- read_lines("/datadrive/effective_stack_data_date") %>% as.Date() + months(1)
max_date_qtr <- paste0(year(max_date),".",quarter(max_date))
df2 <- readRDS(file.path(source_dir, "reponame_quarterly_df.rds"))

prepData <- function(df, lagstring="|^L18|^L19|^DL18", maxdate=max_date, panel="all", timepoly=c(3,2,1), 
                     selectedVars=sv, squaredterms=TRUE, interactions=TRUE, outcomevar="notable_badge",
                     doPlot=TRUE, doLatex=TRUE, outcome_growth=FALSE, outcome_growth_prefix="L18") {
  
  hasDate <- df$namewithowner_l[df$date==maxdate] %>% unique()
  
  df2 <- df %>% ungroup() %>% filter(namewithowner_l %in% hasDate) %>%
    select(namewithowner_l, period, foundation, notable_badge, notable_badge_cumsum, notable_badge_3mth, famous_badge, famous_badge_cumsum, famous_badge_3mth, cumstars,
           matches(paste0("^language|^owner_type", lagstring))) %>% 
    group_by(namewithowner_l)
  
  if (panel=="all") {
    df2 %<>% na.omit %>% ungroup()
  } else if (panel=="mostrecent") {
    df2 %<>% filter(period==max(period)) %>% na.omit() %>% ungroup()
  } else if (panel=="secondmostrecent") {
    df2 %<>% filter(period==max(period)-1) %>% na.omit() %>% ungroup()
  } else {
    df2 %<>% filter(period==max(period)-as.numeric(panel)) %>% na.omit() %>% ungroup()
  }
  
  df3 <- df2 %>% select(-namewithowner_l) %>% 
    select(-matches("notable_badge"), -matches("famous_badge"), -matches("cumstars"))
  
  if (timepoly==3) {
    ## adds 3rd degree polynomial in time (period)
    period_poly <- data.frame(period=df3$period, period_2=df3$period^2, period_3=df3$period^3)
  } else if (timepoly==2) {
    period_poly <- data.frame(period=df3$period, period_2=df3$period^2)
  } else {
    period_poly <- data.frame(period=df3$period)
  }
  
  df3 %<>% select(-period)
  df3 %<>% select(-owner_typeUser)
  
  languages <- df3 %>% select(matches("^language"))
  df3 %<>% select(-matches("^language"))
  
  if (squaredterms) {
    ## adds squared terms for all continuous variables
    squared_terms <- df3 %>% 
      select(-foundation, -owner_typeOrganization) %>%
      select(selectedVars)
    squared_terms <- squared_terms*squared_terms
    colnames(squared_terms) <- paste0(colnames(squared_terms), "_2")
  } else {
    squared_terms <- df3 %>% 
      select(-foundation, -owner_typeOrganization) %>%
      select(selectedVars)
    squared_terms <- squared_terms
  }
  
  df3$foundation <- ifelse(df3$foundation, 1, 0)
  
  if (interactions) {  
    ## adds interaction between foundation and other variables
    ## adds all 2-way interactions
    all_ints <- model.matrix(~.^2-1, data = df3 %>% select("foundation", "owner_typeOrganization", selectedVars))
    x <- as.matrix(cbind(all_ints, squared_terms, languages, period_poly))
  } else {
    x <- as.matrix(cbind(squared_terms, languages, period_poly))
  }
  
  ## compute highest correlated variables and return dataframe for inspection
  z <- cor(squared_terms)
  z[lower.tri(z,diag=TRUE)]=NA  #Prepare to drop duplicates and meaningless information
  z=as.data.frame(as.table(z))  #Turn into a 3-column table
  z=na.omit(z)  #Get rid of the junk we flagged above
  z=z[order(-abs(z$Freq)),]    
  
  ## collect corresponding outcome variable
  if (outcome_growth) {
    y <- ((df2[,outcomevar]+1) / (df2[, paste0(outcome_growth_prefix, ".", outcomevar)]+1)) %>% pull(outcomevar)
    #y <- (y + abs(min(y))+1) %>% log()
  } else {
    y <- df2 %>% pull(outcomevar) %>% add(1) %>% log()
  }
  
  corevars <- df3 %>% 
    select(-foundation, -owner_typeOrganization) %>%
    select(selectedVars)
  
  if (doLatex) {
    class(corevars) <- "data.frame"
    cap4 <- "Sample characteristics: continuous variables."
    tableContinuous(corevars, cap = cap4, lab = "tab: cont1", longtable = TRUE, prec = 4)
  }
  
  if (doPlot) {
    plot(density(y, na.rm=T), main="Density of Dependent Variable")
  }
  
  return(list(x=x, y=y, z=z, namewithowner_l=df2$namewithowner_l))
}

## all the stuff after the model is run
postLasso <- function(lasso_res, latex=FALSE, doPlots=TRUE, xyz, lambda_choice=c("lambdamin", "lambdamin1se")) {
  lambda_choice <- match.arg(lambda_choice)
  
  fit <- lasso_res
  bestModel <- fit$glmnet.fit
  lambdamin <- fit$lambda.min
  lambdamin1se <- fit$lambda.1se
  
  lambda <- ifelse(lambda_choice=="lambdamin", lambdamin, lambdamin1se)
  
  message("Minimum lambda: ", lambdamin)
  message("Minimum lambda 1 s.e. away: ", lambdamin1se)
  
  if (doPlots) {
    par(mfrow=c(1,1), mar=c(4.5,4.5,1,3.1))
    plot(fit, main = "LASSO Cross-validation results")
  }
  
  message("Minimum cross-validated MSE: ", min(fit$cvm))
  
  if (doPlots) {
    par(mfrow=c(1,1), mar=c(4.5,4.5,1,10))
    plot(bestModel)
    vnat=coef(bestModel)
    vnat=vnat[-1,ncol(vnat)]
    axis(4, at=vnat,line=-.5,label=names(vnat),las=1,tick=FALSE, cex.axis=0.5, col.axis = "#000000")
    
    plot(bestModel, xvar="lambda", label=F)
    axis(4, at=vnat,line=-.5,label=names(vnat),las=1,tick=FALSE, cex.axis=0.5, col.axis = "#000000")
  }
  
  message("Most important variables:")
  var.imp <- varImp(bestModel, lambda=lambda)
  var.imp$variable = rownames(var.imp)
  var.imp %>% 
    mutate(Overall=round(Overall,3)) %>% 
    rename(Importance=Overall) %>%  
    arrange(desc(Importance)) %>% 
    head(20) %>% print()
  
  bmcoef <- coef(fit, s=lambda) %>% as.matrix()
  bmcoef <- data.frame(coefname=rownames(bmcoef), coef=round(bmcoef, 4)) %>% filter(X1!=0)
  bmcoef %<>% arrange(desc(abs(X1))) %>% rename(Label=coefname, Coefficient=X1)
  
  message("Coefficients in order of importance: ")
  head(bmcoef, 50) %>% print()
  
  message("Non-language Coefficients in order of importance: ")
  head(bmcoef %>% filter(!grepl("^language", Label), 15)) %>% print()
  
  if (latex)  print(xtable(bmcoef, digits = 4, caption="Coefficients from LASSO"), include.rownames=FALSE)
  
  ## what does model predict?
  
  message("Prediction Error:")
  if (lasso_res$name=="Binomial Deviance") {
    yhat <- predict(fit, xyz$x, s=lambda,type="response")
    test_set_resp <- confusionMatrix(as.factor(yhat>=.5), 
                                     as.factor(as.logical(xyz$y)), 
                                     positive='TRUE', 
                                     mode="everything")
    print(test_set_resp)
    ROC <- pROC::roc(predictor=yhat[,1],response=xyz$y)
    print(ROC$auc)
    test_set_resp <- c(test_set_resp, ROC$auc)
  } else {
    yhat <- predict(fit, xyz$x, s=lambda)
    test_set_resp <- postResample(yhat, xyz$y)
    print(test_set_resp)
    yhat <- yhat %>% exp %>% subtract(1)
  }
  predDF <- data.frame(namewithowner_l=xyz$namewithowner_l, notable_badge=xyz$y, notable_badge_hat=unname(yhat),  stringsAsFactors = FALSE)
  return(list(predDF=predDF, bmcoef=bmcoef, test_set_resp=test_set_resp))
}

postRanger <- function(ranger_res, xyz) {
  fit <- ranger_res
  
  message("Ranger/RF Results:")
  print(fit)
  
  message("20 Most important variables:")
  bmcoef <- sort(importance(x=fit$finalModel), decreasing = T) 
  bmcoef %>% head(20) %>% print()
  
  message("Test Set Predictions:")
  
  if (fit$modelType=="Classification") {
    y_pred <- predict(fit, xyz$x, type="prob")$HIGH
    test_set_resp <- confusionMatrix(as.factor(ifelse(y_pred >= .5, "HIGH", "LOW")), 
                                     as.factor(xyz$y), 
                                     positive="HIGH", 
                                     mode="everything") 
    print(test_set_resp)
    ROC <- pROC::roc(predictor=y_pred, response=xyz$y)
    print(ROC$auc)
    test_set_resp <- c(test_set_resp, ROC$auc)
  } else {
    y_pred <- predict(fit, xyz$x) 
    test_set_resp <- postResample(y_pred, xyz$y)
    print(test_set_resp)
    y_pred  <- y_pred %>% exp() %>% subtract(1)
  }
  
  predDF <- data.frame(namewithowner_l=xyz$namewithowner_l, notable_badge=xyz$y, notable_badge_hat=unname(y_pred),  stringsAsFactors = FALSE)
  return(list(predDF=predDF, bmcoef=bmcoef, test_set_resp=test_set_resp))
}


prepData_qtr <- function(df, lagstring="|^L5|^L6|^D5", maxdate=max_date_qtr, panel="all", timepoly=c(3,2,1), 
                         selectedVars=sv, squaredterms=TRUE, interactions=TRUE, outcomevar="notable_badge",
                         doPlot=TRUE, doLatex=TRUE, outcome_growth=FALSE, outcome_growth_prefix="L5") {
  
  hasDate <- df$namewithowner_l[df$quarter==maxdate] %>% unique()
  
  df2 <- df %>% ungroup() %>% filter(namewithowner_l %in% hasDate) %>%
    select(namewithowner_l, period, foundation, notable_badge, notable_badge_cumsum, notable_badge_3mth, famous_badge, 
           famous_badge_cumsum, famous_badge_3mth, cumstars, DL0L1.notable_badge,
           matches(paste0("^language|^owner_type", lagstring))) %>% 
    group_by(namewithowner_l)
  
  if (panel=="all") {
    df2 %<>% na.omit %>% ungroup()
  } else if (panel=="mostrecent") {
    df2 %<>% filter(period==max(period)) %>% na.omit() %>% ungroup()
  } else if (panel=="secondmostrecent") {
    df2 %<>% filter(period==max(period)-1) %>% na.omit() %>% ungroup()
  } else {
    df2 %<>% filter(period==max(period)-as.numeric(panel)) %>% na.omit() %>% ungroup()
  }
  
  df3 <- df2 %>% select(-namewithowner_l) %>% 
    select(-matches("notable_badge"), -matches("famous_badge"), -matches("^cumstars"))
  
  if (timepoly==3) {
    ## adds 3rd degree polynomial in time (period)
    period_poly <- data.frame(period=df3$period, period_2=df3$period^2, period_3=df3$period^3)
  } else if (timepoly==2) {
    period_poly <- data.frame(period=df3$period, period_2=df3$period^2)
  } else {
    period_poly <- data.frame(period=df3$period)
  }
  
  df3 %<>% select(-period)
  df3 %<>% select(-owner_typeUser)
  
  languages <- df3 %>% select(matches("^language"))
  df3 %<>% select(-matches("^language"))
  
  if (squaredterms) {
    ## adds squared terms for all continuous variables
    squared_terms <- df3 %>% 
      select(-foundation, -owner_typeOrganization) %>%
      select(selectedVars)
    squared_terms <- squared_terms*squared_terms
    colnames(squared_terms) <- paste0(colnames(squared_terms), "_2")
  } else {
    squared_terms <- df3 %>% 
      select(-foundation, -owner_typeOrganization) %>%
      select(selectedVars)
    squared_terms <- squared_terms
  }
  
  df3$foundation <- ifelse(df3$foundation, 1, 0)
  
  if (interactions) {  
    ## adds interaction between foundation and other variables
    ## adds all 2-way interactions
    all_ints <- model.matrix(~.^2-1, data = df3 %>% select("foundation", "owner_typeOrganization", selectedVars))
    x <- as.matrix(cbind(all_ints, squared_terms, languages, period_poly))
  } else {
    x <- as.matrix(cbind(squared_terms, languages, period_poly))
  }
  
  ## compute highest correlated variables and return dataframe for inspection
  z <- cor(squared_terms)
  z[lower.tri(z,diag=TRUE)]=NA  #Prepare to drop duplicates and meaningless information
  z=as.data.frame(as.table(z))  #Turn into a 3-column table
  z=na.omit(z)  #Get rid of the junk we flagged above
  z=z[order(-abs(z$Freq)),]    
  
  ## collect corresponding outcome variable
  if (outcome_growth) {
    y <- ((df2[,outcomevar]+1) / (df2[, paste0(outcome_growth_prefix, ".", outcomevar)]+1)) %>% pull(outcomevar)
    #y <- (y + abs(min(y))+1) %>% log()
  } else {
    if (outcomevar!="DL0L1.notable_badge") {
      y <- df2 %>% pull(outcomevar) %>% add(1) %>% log()
    } else {
      y <- df2 %>% pull(outcomevar) 
    }
  }
  
  corevars <- df3 %>% 
    select(-foundation, -owner_typeOrganization) %>%
    select(selectedVars)
  
  if (doLatex) {
    class(corevars) <- "data.frame"
    cap4 <- "Sample characteristics: continuous variables."
    tableContinuous(corevars, cap = cap4, lab = "tab: cont1", longtable = TRUE, prec = 4)
  }
  
  if (doPlot) {
    plot(density(y, na.rm=T), main="Density of Dependent Variable")
  }
  
  return(list(x=x, y=y, z=z, namewithowner_l=df2$namewithowner_l))
}
df2 %>% saveRDS("/datadrive/peter/df2.rds")
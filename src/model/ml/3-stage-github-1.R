library(tidyverse)
library(lubridate)
library(magrittr)
library(testthat)
library(future)
library(future.apply)
plan(multiprocess, gc = T)
num_cores <- parallel::detectCores()


## original code is indented with three tabs. Some of it will be commented out...


      # library(magrittr)
      # library(dplyr)
      # library(dbplyr)
      # #library(gh)
      # library(reshape2)
      # library(zoo)
      # library(jsonlite)
      # library(httr)
      # library(tidyr)
      # library(stringr)
      # library(RMySQL)
      # library(tidytext)
      # library(DBI)
      # library(parallel)
      # library(testthat)

### Get GH Repo Stars Over Time

      source_dir <- "/datadrive/peter"
      target_dir <- "/datadrive/peter"
      max_date <- read_lines("/datadrive/effective_stack_data_date") %>% as.Date() + months(1)
      min_date <- as.Date("2007-09-01")
      
      res_df <- readRDS(file.path(source_dir, "top107414_repos.rds"))
      
      all_dates <- seq(min_date, max_date, by="month")
      repos <- res_df$nameWithOwner_l %>% unique()
      repo_api_url <- paste0('https://api.github.com/repos/', repos) 



      repo_ids_qy <- paste0("select * from projects where lower(url) in ", dbplyr::escape(repo_api_url))

      ## get ghtorrent repos that match res_df repo urls. There may be more than one repo_id for a given repo url. There may also be zero
      # if (file.exists("/datadrive/peter/repo_res.rds")) {
      #   repo_res <- readRDS(file.path(source_dir, "repo_res.rds"))

      
# run through the projects csvs and keep them if they match project id with the results from the API

source('/ossprediction/src/model/ml/split-apply-combine.R')

proj_cols <- cols(
  id= col_integer(),
  url = col_character(),
  owner_id = col_integer(),
  name = col_character(),
  description = col_character(),
  language = col_character(),
  created_at = col_character(),
  forked_from = col_character(),
  deleted = col_character(),
  updated_at = col_character(),
  notsure = col_character())

proj_cols_remove <- c("forked_from","deleted","notsure")
proj_match_df <- data.frame(url=repo_api_url) # this is just a df of the repo_api_url object


empty <- combine(table="projects",
                 cols_object = proj_cols,
                 out_df = "repo_res",
                 match_df = proj_match_df,
                 remove_cols = proj_cols_remove,
                 concat=FALSE)

repo_res <- concat(table = "projects", out_df = "repo_res") %>% 
  mutate(namewithowner_l = gsub(url, pattern = "https://api.github.com/repos/", replacement=""),
         namewithowner_f = as.factor(namewithowner_l))

expect_gte(nrow(repo_res), 120000, label="Number of rows of repo_res data frame")

repo_res %>% saveRDS("/datadrive/peter/repo_res.rds")


      # repo_res$namewithowner_l <- str_replace(repo_res$url, pattern = "https://api.github.com/repos/", "") %>% tolower()
      # expect_gte(nrow(repo_res), 100000, label="Number of rows of repo_res data frame")
      # saveRDS(repo_res, file=file.path(target_dir, "repo_res.rds"))
        
        
      stars_qy <- paste0("select * from watchers w where w.repo_id in (select id from projects p where id in ", dbplyr::escape(repo_res$id), ")")
      ## get watcher details over time for repos identified as matches
      
      # if (file.exists("/datadrive/peter/all_stars.rds")) {
      #   all_stars <- readRDS(file.path(source_dir, "all_stars.rds"))
      # } else {
      #   con <- getDB()
      #   all_stars <- dbGetQuery(con, stars_qy)
      #   dbDisconnect(con)


watcher_cols <- cols(
  repo_id = col_integer(),
  user_id = col_integer(),
  created_at = col_datetime())
watcher_match_df <- repo_res %>% select(repo_id=id)

empty <- combine(table="watchers",
                     cols_object = watcher_cols,
                     out_df = "all_stars",
                     match_df = watcher_match_df,
                     concat=FALSE) 

all_stars <- concat(table = "watchers", out_df = "all_stars") %>% 
  left_join(repo_res %>% 
              select(repo_id=id,namewithowner_l,namewithowner_f)) %>% 
  mutate(timestamp = as_date(created_at))


# these three commands double the size of the dataset to close to 1GB. 
# then we have the by-day computation, not sure why, or why it's done this way

# - computational complexity of doing this with strings
#     - use date numbers instead of date strings
#     - use factors for the namewithowner_l 

# - why by day at all? assume this is an artifact of the very first build...
# - what is this method of rolling up by by doing some heavy computation that we're going to throw away?




      # all_stars$namewithowner_l <- repo_res$namewithowner_l[match(all_stars$repo_id, repo_res$id)]
      # all_stars$timestamp <- as.POSIXct(all_stars$created_at)
      # all_stars$year_month_day <- format(all_stars$timestamp, "%Y-%m-%d")

      # expect_gte(nrow(all_stars), 5e7, label="Number of rows of all_stars data frame")
      # saveRDS(all_stars, file=file.path(target_dir, "all_stars.rds"))
      # }
  
expect_gte(nrow(all_stars), 6.5e7, label="Number of rows of all_stars data frame")
# expect_equal(ncol(all_stars),6, label="all_stars df cols")
saveRDS(all_stars, file=file.path(target_dir, "all_stars.rds"))

  

      # if (file.exists(file.path(source_dir, "repo_star_counts_by_day_cum.rds"))) {
      #   repo_star_counts_by_day_cum <- readRDS(file.path(source_dir, "repo_star_counts_by_day_cum.rds"))
      # } else {
      #   ## get daily counts

      # repo_star_counts_by_day <- all_stars %>% group_by(namewithowner_l, year_month_day) %>% count() 
      # repo_star_counts_by_day_cum <- repo_star_counts_by_day %>% 
      #   arrange(namewithowner_l, year_month_day) %>% 
      #   group_by(namewithowner_l) %>% 
      #   mutate(cumsum = cumsum(n))
      # 
      # repo_star_counts_by_day_cum$date <- as.POSIXct(repo_star_counts_by_day_cum$year_month_day)


seq.Date(as.Date(min_date),as.Date(max_date), by="month") -> date_seq
datedf <- data.frame(date=date_seq)

repo_star_counts <- all_stars %>%
  group_by(namewithowner_l, year = year(timestamp), month = month(timestamp)) %>%
  summarise(n = n()) %>% 
  arrange(namewithowner_l, year, month) %>% 
  ungroup %>% 
  group_by(namewithowner_l) %>% 
  mutate(cume_stars = cumsum(n))
  
  

#nest and map to cores indexed by the number of rows that need to be created for the full panel 

repo_star_counts %>% 
  group_by(namewithowner_l) %>% 
  nest() %>% 
  mutate(startrows = data %>% map_dbl(nrow), 
         rows_needed = nrow(datedf) - startrows) %>% 
  ungroup %>% 
  arrange(-rows_needed) %>% 
  mutate(cume_dist = cumsum(rows_needed)/sum(rows_needed)) %>% 
  mutate(coremap = findInterval(cume_dist,
                                seq(0,1,(1/(num_cores-1))),
                                all.inside=T)
         ) %>% 
  ungroup %>% 
  group_by(coremap) %>% 
  nest() -> coremap
  
coremap %>% 
  mutate(data = data %>% 
           map(~ future(.x %>% 
                          mutate(data = data %>% 
                                   map(~ .x %>% 
                                         mutate(date = make_date(year, month, 1L)) %>% 
                                         select(-year,-month) %>% 
                                         right_join(datedf) %>% 
                                         arrange(date) %>% 
                                         fill(.direction = "down"))))) %>% 
           values) %>% 
  unnest %>% unnest %>% 
  select(-coremap,-startrows,-rows_needed,-cume_dist) -> df_of_repos


#expect the rows to be some percentage range within the number of unique URLs of repo_res
#in this case its 
# > ((nrow(repo_star_counts_2))/(repo_res$url %>% unique %>% length))-1
# [1] -0.001067999



      ## convert to long data frame and rename columns
      # df_of_repos <- bind_rows(list_of_repo_ts)
      # df_of_repos %<>% rename(n=cumsum)

#      expect_gte(nrow(df_of_repos), 1e7, label="Number of rows of df_of_repos data frame")
      saveRDS(df_of_repos, file=file.path(target_dir, "df_of_repos.rds"))
      #}



#      expect_gte(nrow(repo_star_counts_by_day_cum), 2e7, label="Number of rows of repo_star_counts_by_day_cum data frame")
#      saveRDS(repo_star_counts_by_day_cum, file=file.path(source_dir, "repo_star_counts_by_day_cum.rds"))
      #  }
  
      # ## make complete time series of cumulative stars for each repo
      #   
      #   if (file.exists(file.path(source_dir, "df_of_repos.rds"))) {
      #     df_of_repos <- readRDS(file.path(source_dir, "df_of_repos.rds"))
      #   } else {
      #     cl <- makeCluster(getOption("mc.cores", n.cores))
      #     clusterExport(cl, c("repo_star_counts_by_day_cum", "all_dates"))
      #     clusterEvalQ(cl, {
      #       library(dplyr)
      #       library(magrittr)
      #     })
      #     
      #     tt <- Sys.time() ## This takes a long time: 9.3 hours. Surely can refactor to speed up somehow
      #     list_of_repo_ts <- parLapply(cl, unique(repo_star_counts_by_day_cum$namewithowner_l), function(xx) {
      #       raw_data <- repo_star_counts_by_day_cum %>% filter(namewithowner_l==xx) %>% 
      #         select(namewithowner_l, cumsum, date) %>% 
      #         mutate(date=format(date, "%Y-%m-01")) %>%
      #         arrange(date) %>% 
      #         group_by(date) %>% 
      #         summarize(cumsum=max(cumsum))
      #       all_dates_frame <- data.frame(list(date=all_dates))
      #       merged_data <- merge(all_dates_frame, raw_data, all=T) %>% select(date, cumsum) %>% mutate(namewithowner_l=xx)
      #       return(merged_data)
      #     })
      #     Sys.time() - tt
      #     stopCluster(cl)
      #     
      #     ## convert to long data frame and rename columns
      #     df_of_repos <- bind_rows(list_of_repo_ts)
      #     df_of_repos %<>% rename(n=cumsum)
      expect_gte(nrow(df_of_repos),15e6 , label="Number of rows of df_of_repos data frame")
      saveRDS(df_of_repos, file=file.path(target_dir, "df_of_repos.rds"))
      # }
  
  
  ### RDS created in this file
  # repo_res.rds
  # all_stars.rds
  # repo_star_counts_by_day_cum.rds
  # df_of_repos.rds
  
  
  
  
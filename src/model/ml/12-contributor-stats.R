library(magrittr)
library(dplyr)
library(dbplyr)
library(zoo)
library(tidyr)
library(stringr)
library(testthat)

#### Create order statistics for committer/authors associated with a repo in month t

### Specifically, what percentage of committers/authors in month X were also committers/authors in top 1000 projects 
### as of month X as measured by cumulative stars?

## First get repos and cumstars and select top N each month.
## N is 1000 and then 10000
source_dir <- "/datadrive/peter"
target_dir <- "/datadrive/peter"

for (n_rank in c(1000, 10000)) {
  
  df_of_repos <- readRDS(file.path(source_dir, "df_of_repos.rds"))
  nrow(df_of_repos)
  nwo_factor_map <- readRDS("/datadrive/peter/repo_res.rds") %>% 
    select(namewithowner_l,namewithowner_f) %>% distinct()
  df_of_repos %<>% inner_join(nwo_factor_map)
  nrow(df_of_repos)
  
  commit_type_df <- readRDS(file.path(source_dir, "commit_type_df.rds"))
#  commit_type_df %<>% mutate(date=paste0(date,"-01"))
  
  top_by_month <- df_of_repos %>% group_by(date) %>%
    arrange(desc(n)) %>%
    filter(row_number() <= n_rank)
  
  top_by_month %<>% arrange(desc(date), n)
  
  top_by_month <- na.omit(top_by_month)
  
  top_by_month_list <- lapply(unique(top_by_month$date), function(xx) {
    return(top_by_month %>% filter(date==xx) %>% pull(namewithowner_f))
  })
  names(top_by_month_list) <- unique(top_by_month$date)
  
  rm(df_of_repos)
  
  ## Second, for each repo in each month, determine who committers/authors were and see if those committers/authors committed to top repos
  ### identify and flag top repos as new column
  commit_type_df$top_repo <- 0
  for (ii in names(top_by_month_list)) {
    commit_type_df$top_repo[commit_type_df$date==ii & commit_type_df$namewithowner_f %in% top_by_month_list[[ii]]] <- 1
  }
  
  ## Next create list of users in each month that contributed to top repos
  top_repo_author <- lapply(names(top_by_month_list), function(xx) {
    commit_type_df %>% filter(date==xx, top_repo==1, type=="author") %>% pull(userid) %>% unique()
  })
  names(top_repo_author) <- unique(top_by_month$date)
  
  top_repo_committer <- lapply(names(top_by_month_list), function(xx) {
    commit_type_df %>% filter(date==xx, top_repo==1, type=="committer") %>% pull(userid) %>% unique()
  })
  names(top_repo_committer) <- unique(top_by_month$date)
  
  ### identify and flag top repo authors/committers as new column
  commit_type_df$top_repo_author <- 0
  for (ii in names(top_by_month_list)) {
    commit_type_df$top_repo_author[commit_type_df$date==ii & commit_type_df$userid %in% top_repo_author[[ii]]] <- 1
  }
  
  commit_type_df$top_repo_committer <- 0
  for (ii in names(top_by_month_list)) {
    commit_type_df$top_repo_committer[commit_type_df$date==ii & commit_type_df$userid %in% top_repo_committer[[ii]]] <- 1
  }
  
  if (n_rank==1000) {
    ### finally create summary for each repo-month n_rank=1000
    a_toprepo <- commit_type_df %>% filter(type=="author") %>% group_by(namewithowner_f, date) %>% summarize(frac_top1000_author=weighted.mean(top_repo_author, count, na.rm=T))
    c_toprepo <- commit_type_df %>% filter(type=="committer") %>% group_by(namewithowner_f, date) %>% summarize(frac_top1000_committer=weighted.mean(top_repo_committer, count, na.rm=T))
    
    ## join DFs
    top_repo_acs <- a_toprepo %>% full_join(c_toprepo, by=c("namewithowner_f", "date"))
    
    ## save
    expect_gte(nrow(top_repo_acs), 400000, label="Number of rows in toprepo_acs_1000")
    saveRDS(top_repo_acs, file.path(target_dir, "toprepo_acs_1000.rds"))
  } else if (n_rank==10000) {
    ### finally create summary for each repo-month n_rank=10000
    a_toprepo <- commit_type_df %>% filter(type=="author") %>% group_by(namewithowner_f, date) %>% summarize(frac_top10000_author=weighted.mean(top_repo_author, count, na.rm=T))
    c_toprepo <- commit_type_df %>% filter(type=="committer") %>% group_by(namewithowner_f, date) %>% summarize(frac_top10000_committer=weighted.mean(top_repo_committer, count, na.rm=T))
    
    ## join DFs
    top_repo_acs <- a_toprepo %>% full_join(c_toprepo, by=c("namewithowner_f", "date"))
    
    ## save
    expect_gte(nrow(top_repo_acs), 400000, label="Number of rows in toprepo_acs_01000")
    saveRDS(top_repo_acs, file.path(target_dir, "toprepo_acs_10000.rds"))
  }
}

### RDS created in this file
# toprepo_acs_1000.rds
# toprepo_acs_10000.rds



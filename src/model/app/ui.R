library(shiny)
library(zoo)

shinyUI(fluidPage(
  
  # Application title
  titlePanel("OSS Model Explorer (Beta)"),
  
  # Sidebar with a slider input for number of bins 
  sidebarLayout(
    sidebarPanel(
      radioButtons("modelType",
                   "Model Type", 
                   choices=c("Notable Badge Levels"="levels", "Notable Badge Risers"="risers"),
                   selected="risers"
      ),
      helpText("The Levels model predicts the number of new Stack Overflow notable badges Q quarters in the future. 
               The Risers model predicts a two-fold or better rise in the number of notable badges over Q quarters."),
      selectInput("predict_quarter",
                  "Modeling Period (Q)",
                  choices = c("Six Quarters"="6", "Four Quarters"="4", "Two Quarters"="2"), 
                  selected = "6"
      ),
      h3("Model Summary"),
      h4("Random Forest Model"),
      h5("Test Set Performance"),
      tableOutput("ModelPerformance")
      ),
    
    mainPanel(
      
      tabsetPanel(
        type="tabs",
        tabPanel("Model", 
                 h3("Most Important Predictors"),
                 p("Ln refers to n-quarter lag. DLnLj refers to change between nth- and jth-quarter lag."),
                 dataTableOutput("ModelCoefficients"),
                 br(),
                 
                 h3(paste0("Test Set Predictions Data Table - ", format(as.yearqtr(as.character(most_recent_quarter), format = "%Y.%q")))),
                 downloadLink("dl_test_set_predictions", label = HTML(paste("Download Table", icon("download")))),
                 dataTableOutput("ModelDataTable")
                 ),
        tabPanel("LIME for Test Set",
                 br(),
                 selectInput("repo_testset",
                             "Select Repository for LIME inspection",
                             choices = "vscode::microsoft/vscode", 
                             selected = "vscode::microsoft/vscode",
                             selectize=T
                 ),
                 helpText("The list is in the form tag::owner/repo, where owner/repo is the GitHub repo that represents the Stack Overflow tag."),
                 
                 h2("LIME (Local Interpretable Model-agnostic Explanations)"),
                 p("LIME fits local linear approximations to explain black box machine learning models such as Random Forests. 
                   The plot below shows which model features support or contradict the model's prediction. For Levels models, the prediction is in logarithms.
                   For Risers models, the prediction shows a probability. 
                   N.B. It is likely that the LIME probability shown is different from the original Random Forest probability as the plot shows
                   the probability based on the local linear approximation to the original model."),
                 p("See more on ", a("Github", href="https://github.com/marcotcr/lime")),
                 textOutput("reponame_testset"),
                 plotOutput("limePLOT_testset"),
                 downloadLink("dl_lime_testset", label = HTML(paste("Download LIME Results", icon("download")))),
                 br(),
                 
                 h2("Who worked on this repo?"),
                 downloadLink("dl_users_testset", label = HTML(paste("Download Table", icon("download")))),
                 dataTableOutput("UserDataTable_testset")
                 ),
        tabPanel("Predictions",
                 br(),
                 helpText("Table contains exact and partial matches between GitHub and StackOverflow. Use ExactMatch column to filter for only exact matches."),
                 downloadLink("dl_future_predictions", label = HTML(paste("Download Table", icon("download")))),
                 dataTableOutput("FuturePredictionDataTable")),
        tabPanel("LIME for Predictions",
                 br(),
                 selectInput("repo_predictions",
                             "Select Repository for LIME inspection",
                             choices = "vscode::microsoft/vscode", 
                             selected = "emberjs::emberjs/ember.js",
                             selectize=T
                 ),
                 helpText("The list is in the form tag::owner/repo, where owner/repo is the GitHub repo that represents the Stack Overflow tag.
                          The table should be read as the prediction of notable badges (or riser likelihood) for the 'tag'. "),
                 
                 h2("LIME (Local Interpretable Model-agnostic Explanations)"),
                 p("LIME fits local linear approximations to explain black box machine learning models such as Random Forests. 
                   The plot below shows which model features support or contradict the model's prediction. For Levels models, the prediction is in logarithms.
                   For Risers models, the prediction shows a probability. 
                   N.B. It is likely that the LIME probability shown is different from the original Random Forest probability as the plot shows
                   the probability based on the local linear approximation to the original model."),
                 p("See more on ", a("Github", href="https://github.com/marcotcr/lime")),
                 textOutput("reponame_predictions"),
                 plotOutput("limePLOT_predictions"),
                 downloadLink("dl_lime_predictions", label = HTML(paste("Download LIME Results", icon("download")))),
                 br(),
                 
                 h2("Who worked on this repo?"),
                 downloadLink("dl_users_predictions", label = HTML(paste("Download Table", icon("download")))),
                 dataTableOutput("UserDataTable_predictions")
                 )
        
      )
    )
  )
))
library(tidyverse)
library(lubridate)
library(magrittr)
library(zoo) #used somewhere towards the bottom for an na.fill
library(future)
library(future.apply)
plan(multiprocess, gc=T)


filepattern="^DF_(27|28)_([0-9]+)_([0-9]+).rds$"

df <- data.frame(files = list.files('/datadrive/badges/processed'), stringsAsFactors=F) %>%
  mutate(badge_num = files %>% gsub(pattern=filepattern, replacement="\\1"),
         start_date_epoch = files %>% gsub(pattern=filepattern, replacement="\\2") %>% as.numeric,
         end_date_epoch = files %>% gsub(pattern=filepattern, replacement="\\3") %>% as.numeric,
         start_datetime = as.POSIXct(start_date_epoch,origin = "1970-01-01",tz = "UTC"),
         end_datetime = as.POSIXct(end_date_epoch,origin = "1970-01-01",tz = "UTC"))

df %>% saveRDS('/datadrive/badge_metadata.rds')

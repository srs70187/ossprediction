#!/bin/bash
set -euxo pipefail

############################################
################Parameters##################        
#                                         ##   
#first argument is the badge number.      ##
#accepted values: 27, 28                  ##
#27 is notable badge                      ##
#28 is famous badge                       ##                         
#ELSE IS processed DFs                    ##
############################################
############################################
export stor_rg=ossdatastore-dev
export stor_location=westus
export stor_name=datastore055

az group create --name ${stor_rg} --location ${stor_location} > ${stor_rg}-rg.json

az storage account create --name ${stor_name} --resource-group ${stor_rg} --sku Standard_LRS > ${stor_name}-storage-account.json

az storage account keys list -n ${stor_name} -g ${stor_rg} > ${stor_name}-keys.json

storage_account_name=$(cat ${stor_name}-storage-account.json | jq -r '.name')
storage_account_key=$(cat ${stor_name}-keys.json | jq -r '.[] | select(.keyName=="key1") | .value')
storage_account_id=$(cat ${stor_name}-storage-account.json | jq -r '.id')

storage_account_endpoint=$(cat ${stor_name}-storage-account.json | jq -r '.primaryEndpoints.blob')

storage_account_cstring=$(az storage account show-connection-string --name ${stor_name} -g ${stor_rg} | jq '.connectionString')

echo $storage_account_name
echo $storage_account_key
echo $storage_account_id
echo $storage_account_endpoint

if [ $1 = 27 ]; then
    export container_name=notable
    echo "Processing Notable Badges"
    export container_endpoint=${storage_account_endpoint}/${container_name}
    echo $container_endpoint
    az storage container create --name ${container_name} --connection-string ${storage_account_cstring}
    az storage container list --connection-string $storage_account_cstring > ${container_name}-container.json

    az storage blob upload-batch --connection-string ${storage_account_cstring} --destination ${container_endpoint} --type block --source ${badge_dir}

elif [ $1 = 28 ]; then
    export container_name=famous
    echo "Processing Famous Badges"
    export container_endpoint=${storage_account_endpoint}/${container_name}
    echo $container_endpoint
    az storage container create --name ${container_name} --connection-string ${storage_account_cstring}
    az storage container list --connection-string $storage_account_cstring > ${container_name}-container.json
    az storage blob upload-batch --connection-string ${storage_account_cstring} --destination ${container_endpoint} --type block --source ${badge_dir}
else
    export container_name=processed
    echo "Processing...processed..."
    export container_endpoint=${storage_account_endpoint}/${container_name}
    echo $container_endpoint
    az storage container create --name ${container_name} --connection-string ${storage_account_cstring}
    az storage container list --connection-string $storage_account_cstring > ${container_name}-container.json
    
    for f in `ls ${badge_dir} | grep DF`; 
    do 
        az storage blob upload --container-name ${container_name} --file ${badge_dir}/$f --name $f --connection-string ${storage_account_cstring} --type block
    done
fi
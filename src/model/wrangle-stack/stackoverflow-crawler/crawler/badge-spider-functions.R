library(selectr)
library(rvest)
library(httr)
library(dplyr)
library(purrr)

# args <- commandArgs(trailingOnly = TRUE)
# vm_id = 1
# badge_id = 28
# vms = 1
# badge_dir = "/tmp"

# badge_dir <- sprintf('%s/badge-%s',html_dir,badge_id)
# if (badge_id==27) {
#   badge_dir <- "/badge-dir/notable"
# }

# if (badge_id==28) {
#   badge_dir <- "/badge-dir/famous"
# }

relevant_badges <- tibble(badge_num = c(23,24,25,26,27,28),
                          badge_name = c("Nice Answer","Good Answer","Great Answer",
                                         "Popular Question","Notable Question","Famous Question"),
                          short_desc = c("Answer Score of 10","Answer Score of 25","Answer Score of 100",
                                         "1000 Views","2500 Views","10000 Views")) %>% 
  mutate(url = sprintf("https://stackoverflow.com/help/badges/%s",badge_num))

query_last_page <- function(bn
                            ,badgedf = relevant_badges
                            ,last_page_css = ".dots+ a .page-numbers"
                            ,last_page_xpath = "//*+[contains(concat( ' ', @class, ' ' ), concat( ' ', 'dots', ' ' ))]//a//*[contains(concat( ' ', @class, ' ' ), concat( ' ', 'page-numbers', ' ' ))]"
                            ,total_count_css = ".single-badge-count"
                            ,question_link_css = ".single-badge-summary :nth-child(1)"
                            ,date_awarded_css = ".single-badge-awarded"
                            ,user_details_css = ".user-details a") {
  
  df <- relevant_badges %>% 
    filter(badge_num==bn)
  html <- xml2::read_html(df$url)
  
  html %>% 
    rvest::html_nodes(css = last_page_css) %>% 
    rvest::html_text() %>% 
    as.numeric -> last_page
  
  return(last_page)
  
}

worker_ids <- function(n,workers) {
  perworker <- floor(n/workers)
  rep(seq_len(workers),perworker+1) -> tmpid
  return(tmpid[1:n] %>% 
           sort)
}

#query_last_page(23)

stack_url <- function(page,badge) {
  u <- parse_url("https://stackoverflow.com/help/badges/28/?page=1")
  if(page==0) {
    return(list(url = u %>% modify_url(query = NULL,
                                       path = paste0("help/badges/",badge)),
                fname = paste0("html_badge_",badge,"_page_main",".html")))
  }
  
  if(!page==0) {
    return(list(url = u %>% modify_url(query = list(page=page),
                                       path = paste0("help/badges/",badge)),
                fname = paste0("html_badge_",badge,"_page_",page,".html")))
    
  }
  
}

stack_html <- function(r,path=badge_dir) {
  
  u <- r$url
  f <- r$fname
  safe_read_html <- safely(read_html)
  tmp_read <- safe_read_html(sprintf("/%s/%s",path,f))
  if(is.null(tmp_read$error)) {
    return("Found on disk")
  }
  if(!is.null(tmp_read$error)) {
    html <- read_html(u)
    html %>% write_html(sprintf("/%s/%s",path,f))
    Sys.sleep(2.5)
    return("Scraped new html")
  }
}
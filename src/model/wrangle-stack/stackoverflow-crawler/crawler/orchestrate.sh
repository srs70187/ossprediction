#!/bin/bash

#TODO:
#turn this into either a makefile or include some parameters that can be set from another makefile

##set this correctly!
export badge_id=28

export vmss_rg=nbscrape
export vmss_name=nbvmss
export location=westus
export instance_count=1
export vm_sku=Standard_DS2_v2
export vm_admin=ossadmin
export ssh_key_path=@/root/.ssh/id_rsa.pub

# az group create -n $vmss_rg --location $location
# az vmss create -n ${vmss_name} -g ${vmss_rg} --image UbuntuLTS --instance-count ${instance_count} --vm-sku ${vm_sku} --authentication-type ssh --public-ip-per-vm --admin-username ${vm_admin} --ssh-key-value ${ssh_key_path} > nbvmss-create.json
# az vmss list-instance-public-ips --name $vmss_name --resource-group $vmss_rg | jq -r '.[].ipAddress' > vmss-ip-addresses.txt

readarray vmss_ips < vmss-ip-addresses.txt
export num_ips=${#vmss_ips[@]}
echo $num_ips
#this will copy the necessary files over to each vm in the scale set
export config_dir=/msft-oss-eng/az/cli/config

for (( i=0; i<${num_ips}; i++ ));
do
    ip=$(echo ossadmin@${vmss_ips[$i]})
    echo $ip

    scp -r -o StrictHostKeyChecking=no $config_dir $ip:/home/ossadmin
    scp -r -o StrictHostKeyChecking=no . $ip:/home/ossadmin    
    ssh -o StrictHostKeyChecking=no $ip 'bash -s' < vmss-deps.sh &
done

#does the rest of the processing in parallel on each vm of the scale set
#don't press enter until it's done!
for (( i=0; i<${num_ips}; i++ ));
do
    ip=$(echo ossadmin@${vmss_ips[$i]})
    echo $ip
    ssh -o StrictHostKeyChecking=no $ip bash -c "'
    
    export vm_id=$(( i + 1 ))
    export badge_id=${badge_id}
    export badge_id2=0
    export vms=${num_ips}
    
    if [ ${badge_id} == 27 ]; then
        export badge_dir=/badge-dir/notable
    elif [ ${badge_id} == 28 ]; then
        export badge_dir=/badge-dir/famous
    else
        export badge_dir=/home/ossadmin
    fi
    
    #make build && make run-spider
    #./copy-to-container.sh ${badge_id}
    make build-html2df && make run-html2df    
    ./copy-to-container.sh 0

    '" &
done

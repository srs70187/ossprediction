#!/bin/bash
set -euxo pipefail

source install-deps.source

install_apt_libraries
install_azcli
install_docker_compose
if which docker > /dev/null; then echo Docker already installed; else install_docker; fi


export service_principal=$(cat service-principal-metadata.json | jq -r '.[].appId')
export password=$(cat .spkey)
export tenant=$(cat service-principal-metadata.json | jq -r '.[].additionalProperties.appOwnerTenantId')

az login --service-principal \
    -u ${service_principal} \
    -p ${password} \
    -t ${tenant}
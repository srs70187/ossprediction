library(DBI)
library(tidyverse)
library(lubridate)
library(magrittr)
library(future)
library(future.apply)
plan(multiprocess, gc=T)
 
con <- dbConnect(RMariaDB::MariaDB(), host = "stackoverflow",
                 user = "stack", password = "overflow", dbname = "stackoverflow")


votes_local <- dbGetQuery(con,"select Id, PostId, VoteTypeId, CreationDate from Votes where VoteTypeId in (2,3,5)") 
votes_local %>% saveRDS("/datadrive/stackoverflow-current/votes_local.rds")

votes_local %>% 
  group_by(PostId,
           year=year(CreationDate),
           month=month(CreationDate),
           VoteTypeId) %>% 
  summarise(count=n()) -> mvotes

mvotes %>% saveRDS('/datadrive/stackoverflow-current/mvotes.rds')

upvotes <- mvotes %>% 
  ungroup %>%
  filter(VoteTypeId==2) %>%
  mutate(vote_type = "upvote") %>% 
  select(-VoteTypeId) %>% 
  spread(vote_type,count)
upvotes %>% saveRDS("/datadrive/stackoverflow-current/upvotes.rds")

downvotes <- mvotes %>% 
  ungroup %>%
  filter(VoteTypeId==3) %>%
  mutate(vote_type = "downvote") %>% 
  select(-VoteTypeId) %>% spread(vote_type,count)
downvotes %>% saveRDS("/datadrive/stackoverflow-current/downvotes.rds")


favorites <- mvotes %>% 
  ungroup %>%
  filter(VoteTypeId==5) %>%
  mutate(vote_type = "favorite") %>% 
  select(-VoteTypeId) %>% 
  spread(vote_type,count)
favorites %>% saveRDS("/datadrive/stackoverflow-current/favorites.rds")

mv2 <- upvotes %>% full_join(downvotes)
mv2 %>% nrow
mv2 %>% full_join(favorites) -> mv2
mv2 %>% nrow
mv2 %>% 
  ungroup %>%
  replace_na(list(downvote=0,favorite=0,upvote=0)) -> mv2

mv2 %>% saveRDS("/datadrive/stackoverflow-current/mvotes_wide.rds")

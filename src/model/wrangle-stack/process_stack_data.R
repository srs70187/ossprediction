#############################################################################
#############################################################################

##stack_v1_ceiling_date needs to be parameterized!

#ceiling_date <- latest_SO_upload_date #... how do you want to pass this in?
#we could just set it with a query to the internet archive:

#this will work as long as they don't change the layout of the site
#alternatively the general date range from this clickthrough link would work and probably
#be more reliable in the long term. https://archive.org/download/stackexchange



#these packages all come preinstalled with the tidyverse image, so will load them all:
library(selectr)
library(rvest)
library(httr)
library(dplyr)
library(purrr)
library(xml2)

internet_archive_url <- "https://archive.org/details/stackexchange"
html <- xml2::read_html(internet_archive_url)
selector <- ".actions-ia+ .key-val-big"

stack_data_dump_date <- html %>% 
  rvest::html_nodes(".key-val-big") %>% 
  html_nodes("span") %>% 
  .[[3]] %>% 
  html_text %>% as.Date

effective_stack_data_date <- lubridate::floor_date(stack_data_dump_date,unit = "month")
effective_stack_data_date %>% write_lines("/datadrive/effective_stack_data_date")


#############################################################################
#############################################################################

library(tidyverse)
library(lubridate)
library(magrittr)
library(future)
library(zoo) #used somewhere towards the bottom for an na.fill


filepattern="^DF_(27|28)_([0-9]+)_([0-9]+).rds$"

df <- data.frame(files = list.files('/datadrive/badges'), stringsAsFactors=F) %>%
  mutate(badge_num = files %>% gsub(pattern=filepattern, replacement="\\1"),
         start_date_epoch = files %>% gsub(pattern=filepattern, replacement="\\2") %>% as.numeric,
         end_date_epoch = files %>% gsub(pattern=filepattern, replacement="\\3") %>% as.numeric,
         start_datetime = as.POSIXct(start_date_epoch,origin = "1970-01-01",tz = "UTC"),
         end_datetime = as.POSIXct(end_date_epoch,origin = "1970-01-01",tz = "UTC"))

df %>% saveRDS('/datadrive/badge_metadata.rds')


### process

df <- readRDS("/datadrive/badge_metadata.rds")

df %>% head

df %>%
  mutate(data = files %>% map(~ readRDS(paste0('/datadrive/badges/',.x)))) -> dfwithdata

# dfwithdata %>% 
#     filter(badge_num==27)

# dfwithdata %>% filter(badge_num==27) %>% slice(25) -> tmp
# tmp2 <- tmp %>% unnest

# tmp2 %>% 
#     group_by(question_number,user_number,badge_num) %>% 
#     summarise(count=n()) %>%
#     ungroup %>% 
#     arrange(-count) %>% 
#     filter(!is.na(question_number) & !is.na(user_number)) -> dupes

# tmp2 %>% 
#     inner_join(dupes) %>% 
#     filter(count==3) %>% 
#     arrange(badge_num,question_number,user_number) %>% 
#     head %>% 
#     data.frame


### remove duplicates

dfwithdata %>% 
  filter(badge_num==27) %>% 
  unnest -> badge_27_data

badge_27_data %>% nrow

badge_27_data %>% 
  arrange(badge_awarded_date_num,question_number,user_number) %>%
  group_by(badge_awarded_date_num,question_number,user_number) %>% 
  mutate(count=n()) %>% 
  ungroup %>%
  filter(count==1) -> badge_27_data

badge_27_data %>% nrow

library(lubridate)

mbad27 <- badge_27_data %>% 
  filter(!is.na(question_number)) %>%  ## there are a few NA's that need to be removed
  select(post_id=question_number,
         award_date = badge_awarded_date) %>%  #,
  #         user_id=user_number) %>% 
  mutate(year = year(award_date),
         month = month(award_date),
         notable_badge = 1,
         post_id = as.integer(post_id)) %>% 
  select(-award_date)

mbad27 %>% saveRDS("/datadrive/stackoverflow-current/mbad27.rds")




dfwithdata %>% 
  filter(badge_num==28) %>% 
  unnest -> badge_28_data

badge_28_data %>% nrow

badge_28_data %>% 
  arrange(badge_awarded_date_num,question_number,user_number) %>%
  group_by(badge_awarded_date_num,question_number,user_number) %>% 
  mutate(count=n()) %>% 
  ungroup %>%
  filter(count==1) -> badge_28_data

badge_28_data %>% nrow

library(lubridate)

mbad28 <- badge_28_data %>% 
  filter(!is.na(question_number)) %>%  ## there are a few NA's that need to be removed
  select(post_id=question_number,
         award_date = badge_awarded_date) %>%  #,
  #         user_id=user_number) %>% 
  mutate(year = year(award_date),
         month = month(award_date),
         famous_badge = 1,
         post_id = as.integer(post_id)) %>% 
  select(-award_date)

mbad28 %>% saveRDS("/datadrive/stackoverflow-current/mbad28.rds")

mbad27 %>% full_join(mbad28) %>%
  replace_na(list(famous_badge=0,notable_badge=0)) %>%
  ungroup %>%
  arrange(post_id,year,month) %>%
  mutate(post_id = as.integer(post_id) #needs to be integer to join to votes table...
  ) -> mbads
mbads %>% saveRDS("datadrive/stackoverflow-current/mbads.rds")



### votes

library(DBI)
library(tidyverse)
library(lubridate)

dir.create('/datadrive/stackout')
con <- dbConnect(RMariaDB::MariaDB(), host = "stackoverflow",
                 user = "stack", password = "overflow", dbname = "stackoverflow")

#need to sudo apt-get update from the bash of rstudio
#then sudo apt-get install libmariadb-client-lgpl-dev -y

dbListTables(con)

#seq by 10000 of the rows, then q

dbGetQuery(con,"select Id, PostTypeId, AcceptedAnswerId, ParentId, CreationDate, DeletionDate, Score, ViewCount, OwnerUserId, OwnerDisplayName, LastEditorUserId, LastEditorDisplayName, LastEditDate, LastActivityDate, Title, Tags, AnswerCount, CommentCount, FavoriteCount, ClosedDate, CommunityOwnedDate from Posts where PostTypeId=1") %>% 
  saveRDS('/datadrive/stackoverflow-current/stackliteq.rds')

# Just do this 1000 rows at a time and save the csv.

dbListFields(con,"Votes")

# dbGetQuery(con,"select Id, PostTypeId, AcceptedAnswerId, ParentId, CreationDate, DeletionDate, Score, ViewCount, OwnerUserId, OwnerDisplayName, LastEditorUserId, LastEditorDisplayName, LastEditDate, LastActivityDate, Title, Tags, AnswerCount, CommentCount, FavoriteCount, ClosedDate, CommunityOwnedDate from Posts where PostTypeId=1") %>% 
#   saveRDS('/datadrive/stackoverflow-current/stackliteq.rds')
# 
# 
# dbGetQuery(con,"select Id, PostTypeId, AcceptedAnswerId, ParentId, CreationDate, DeletionDate, Score, ViewCount, OwnerUserId, OwnerDisplayName, LastEditorUserId, LastEditorDisplayName, LastEditDate, LastActivityDate, Title, Tags, AnswerCount, CommentCount, FavoriteCount, ClosedDate, CommunityOwnedDate from Posts where PostTypeId=2") %>% 
#   saveRDS('/datadrive/stackoverflow-current/stacklitea.rds')

votes_local <- dbGetQuery(con,"select Id, PostId, VoteTypeId, CreationDate from Votes where VoteTypeId in (2,3,5)")
votes_local %>% saveRDS("/datadrive/stackoverflow-current/votes_local.rds")

library(lubridate)
votes_local %>% 
  group_by(PostId,
           year=year(CreationDate),
           month=month(CreationDate),
           VoteTypeId) %>% 
  summarise(count=n()) -> mvotes

mvotes %>% saveRDS('/datadrive/stackoverflow-current/mvotes.rds')










source('process-some.R')









### mbads to mv2
mbads <- readRDS("/datadrive/stackoverflow-current/mbads.rds")

stackliteq <- readRDS('/datadrive/stackoverflow-current/stackliteq.rds')
stackliteq %>%
  mutate(creation_date=ymd_hms(CreationDate)) %>% 
  select(post_id = Id,
         creation_date,
         post_owner_id=OwnerUserId) -> question_info

question_info %>% saveRDS("/datadrive/stackoverflow-current/question_info.rds")

question_info %>% 
  select(post_id,
         creation_date) %>% 
  mutate(year = year(creation_date),
         month = month(creation_date),
         question_created = 1) %>% 
  select(-creation_date) -> question_info_short

question_info_short %>% saveRDS("/datadrive/stackoverflow-current/question_info_short.rds")

### restart R and only load what's needed because this is a large join

mv2 %>%
  dplyr::rename(post_id=PostId) %>%
  ungroup %>%
  full_join(mbads %>% ungroup) %>%
  full_join(question_info_short) %>%
  replace_na(list(downvote=0,
                  favorite=0,
                  upvote=0,
                  notable_badge=0,
                  famous_badge=0,
                  question_created=0)) %>%
  arrange(post_id,year,month) -> votes_badges

votes_badges %>% saveRDS("/datadrive/stackoverflow-current/votes_badges.rds")



source('requirements.R')
library(magrittr)
library(future)
# plan(multisession)  ## defaults to availableCores() workers
# options(future.globals.maxSize= 13631488000)

votes_badges <- readRDS("/datadrive/stackoverflow-current/votes_badges.rds")

stackliteq <- readRDS("/datadrive/stackoverflow-current/stackliteq.rds")

smart_mod <- function(sequence,mod) return(((sequence-1) %% mod)+1)

stackliteq %>% 
  select(id=Id,tags=Tags) %>% 
  ungroup %>% 
  mutate(worker = smart_mod(seq_along(row_number()),12)) %>% 
  arrange(worker) %>% 
  group_by(worker) %>% 
  nest -> qsample
qsample %>% saveRDS("/datadrive/stackoverflow-current/qsample.rds")


# install.packages("future")
library(tidyverse)
library(future)
plan(multiprocess, gc=TRUE)
qsample <- readRDS("/datadrive/stackoverflow-current/qsample.rds")

qsample %>% 
  mutate(tagx = data %>% map(~ future(.x %>%
                                        mutate(tagx2 = tags %>% 
                                                 map(~ gsub(.x,pattern="^(<{1})(.*)(>{1})$", replacement="\\2") %>%
                                                       str_split("><") %>%
                                                       flatten_chr)) %>% 
                                        select(id,tagx2) %>% 
                                        unnest)) %>% 
           values) %>% 
  select(-data) -> qsf

qsf$tagx %>% reduce(bind_rows) -> unnested_tags
unnested_tags %>% saveRDS("/datadrive/stackoverflow-current/unnested_tags.rds")

unnested_tags %>% 
  group_by(id) %>% 
  summarise(num_tags = n()) -> tut

tut$num_tags %>% describe

tun <- unnested_tags %>% group_by(tagx2) %>% nest()
tun %>% saveRDS("datadrive/stackoverflow-current/tun.rds")

#install.packages("future.apply")
library(tidyverse)
library(future.apply)
plan(multiprocess, gc=T)
options(future.globals.maxSize= 6000000000) #we need 5.7G per worker for globals

tun <- readRDS("/datadrive/stackoverflow-current/tun.rds")

questions_per_tag <- tun %>% 
  mutate(num_questions = data %>% map_dbl(nrow)) %>% 
  arrange(-num_questions) %>% 
  mutate(cume = cumsum(num_questions),
         share = num_questions/sum(num_questions),
         cume_share = cume/sum(num_questions))


#(total_qs <- questions_per_tag$num_questions %>% sum)

# cut the first 75% of the questions off by tag and do those piecemeal
# do the remaining 25% brute force


votes_badges <- readRDS("/datadrive/stackoverflow-current/votes_badges.rds")

bounds_lst <- list(c(0,.25),c(.25,.5),c(.5,.75),c(.75,1))

sets <- future_lapply(seq_along(bounds_lst), function(i) {
  
  lb <- bounds_lst[[i]] %>% min
  ub <- bounds_lst[[i]] %>% max
  
  quad_df <- questions_per_tag %>% 
    filter(cume_share>=lb & cume_share<ub) %>% 
    unnest() %>% 
    rename(post_id=id) %>% 
    left_join(readRDS("/datadrive/stackoverflow-current/votes_badges.rds")) %>% 
    select(-cume,-cume_share)
  
  quad_df %>% 
    saveRDS(sprintf("/datadrive/stackoverflow-current/quartile_%s_tags.rds",i))
  
})

# untangle <- function(nested) {
#   nested %>% 
#     unnest %>%
#     mutate(tagx = tags %>% 
# map(~ future(gsub(.x,pattern="^(<{1})(.*)(>{1})$", replacement="\\2") %>%
#                str_split("><") %>%
#                flatten_chr)) %>%
#              values) %>%
#     select(-tags) %>% 
#     unnest
# }
# 
# qsample %>% 
#   mutate(data = data %>% map(untangle)) %>%
#   unnest -> unnested_tags_0
# 
# qsf$tagx %>% reduce(bind_rows) -> unnested_tags
# unnested_tags %>% saveRDS("data/unnested_tags.rds")


# mutate(tagx = data %>% map(~ future(.x %>% 
#                                       mutate(tagx2 = tags %>% 
#                                                map(~ str_split(.x, pattern="><") %>% 
#                                                      flatten_chr)) %>% 
#                                       select(id,tagx2) %>% 
#                                       unnest)) %>% values) %>%
#   select(-data) -> qsf



# NOTE: pattern of tags are "<a><b><c><d>", which is a different format than 
# it was back in september...
# so first strip the leading < and trailing >
# then split the string on the remaining >< characters that separate the tags
# ... in sep 2017 the tags were separated by a "|"
# qsample %>% 
#   head(1) %>%
#   mutate(tagx = data %>% 
#     map(~ .x %>% 
#       mutate(tagx2 = tags %>% 
#         map(~ .x %>% 
#                 gsub(pattern="^(<{1})(.*)(>{1})$", replacement="\\2") %>% 
#                 str_split("><") %>% 
#                 flatten_chr
#         )
#       ) %>% 
#       select(id,tagx2) %>%
#       unnest)
#   ) %>%  
#   select(-data) -> qsf

# library(future)
# plan(multisession)


# tmp <- qsample %>% slice(1) %>% unnest
# tmp %>%
#   sample_n(5) %>%
#   mutate(tagx = tags %>% map(~ gsub(.x,pattern="^(<{1})(.*)(>{1})$", replacement="\\2") %>% str_split("><") %>% flatten_chr)) %>%
#   select(-tags) %>%
#   unnest




#library(future)
#options(future.globals.maxSize= 13631488000)

create_tag_df <- function(inputdf) {
  
  inputdf %>% 
    ungroup %>% 
    filter(question_created==0) %>% 
    group_by(tagx2,year,month) %>% 
    summarise(questions_upvoted = sum(upvote>0,na.rm=T),
              questions_downvoted = sum(downvote>0,na.rm=T),
              questions_favorited = sum(favorite>0,na.rm=T),
              questions_mixed = sum(((upvote>0|favorite>0) & downvote>0),na.rm=T)) -> tmp1
  
  inputdf %>% 
    select(-num_questions,-share,-post_id) %>% 
    group_by(tagx2,year,month) %>% 
    summarise_all(funs("sum")) %>% 
    left_join(tmp1) %>% 
    ungroup %>% 
    mutate_all(funs(na.fill(.,0))) -> tmp
  
  
  return(tmp)
  
}

unbalanced_panel <- function(inputdf) {
  inputdf %>% 
    ungroup %>% 
    mutate(date = as.Date(sprintf("%s-%s-01",year,month))) %>% 
    group_by(tagx2) %>% 
    mutate(created_date = min(date) %>% as.Date) %>% 
    select(-date) %>% 
    complete(nesting(tagx2),year,month,created_date) %>% 
    ungroup %>% 
    mutate(date = as.Date(sprintf("%s-%s-01",year,month))) %>% 
    filter(date>=created_date,
           date<effective_stack_data_date) %>% 
    select(-date,-created_date) %>% 
    mutate_all(funs(na.fill(.,0))) -> tmp
  
  return(tmp)
}

Tags <- tibble(quartile = seq_len(4),
               path = sprintf("/datadrive/stackoverflow-current/quartile_%s_tags.rds",quartile)) %>% 
  mutate(data = path %>% map(readRDS),
         data = data %>% map(create_tag_df)) %>% 
  select(-path) %>% 
  unnest %>% 
  arrange(tagx2,year,month,-question_created) 

Tags %>% saveRDS("/datadrive/stackoverflow-current/Tags.rds")

yearmons <- data.frame(date=
                         seq.Date(as.Date("2008-01-01"), effective_stack_data_date, "months") %>% as.Date) %>%
  mutate(year=year(date), 
         month=month(date),
         needs_rows = abs(seq_along(date) - length(seq_along(date)))-1) 


months_between <- function(start_ymd,end_ymd) {
  
  return(length(seq(from=floor_date(as.Date(start_ymd)), 
                    to=floor_date(as.Date(end_ymd)), by='month'))-1)
}


Tags %>%
  mutate(date = as.Date(sprintf("%s-%s-01",year,month))) %>%
  group_by(tagx2) %>%
  summarise(start_date = min(date) %>% as.Date,
            has = n_distinct(date)) %>% 
  left_join(yearmons %>% 
              select(start_date=date,
                     needs=needs_rows)) %>%
  ungroup %>% 
  mutate(missing = needs-has,
         missing = ifelse(missing<0,0,missing)) %>% 
  arrange(-missing) %>% 
  mutate(cume_share = cumsum(missing)/sum(missing),
         group = findInterval(cume_share,vec=seq(0,1,.1),rightmost.closed = T)) -> tag_panel_map

tag_panel_map %>% saveRDS("/datadrive/stackoverflow-current/tag_panel_map.rds")

#Tags %>% left_join(tag_panel_map %>% select(tagx2,group)) -> tmp  

Tags %>% 
  left_join(tag_panel_map %>% select(tagx2,group)) %>% 
  group_by(group) %>% 
  nest() -> tmp



plan(multiprocess, gc=T)  ## defaults to availableCores() workers

tmp %>% 
  mutate(data = data %>% map(~ future(unbalanced_panel(.x))) %>% values) -> res

res %>% unnest -> final

final %>% select(-quartile,-group) %>% arrange(tagx2,year,month) -> final

final %>% saveRDS("/datadrive/stackoverflow-current/stack_tag_panel.rds")

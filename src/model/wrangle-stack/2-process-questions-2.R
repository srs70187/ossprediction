library(DBI)
library(tidyverse)
library(lubridate)
library(magrittr)
library(future)
library(future.apply)
plan(multiprocess, gc=T)
options(future.globals.maxSize= 6000000000) 

qsample <- readRDS("/datadrive/stackoverflow-current/qsample.rds")

qsample %>% 
  mutate(tagx = data %>% map(~ future(.x %>%
                                        mutate(tagx2 = tags %>% 
                                                 map(~ gsub(.x,pattern="^(<{1})(.*)(>{1})$", replacement="\\2") %>%
                                                       str_split("><") %>%
                                                       flatten_chr)) %>% 
                                        select(id,tagx2) %>% 
                                        unnest)) %>% 
           values) %>% 
  select(-data) -> qsf

qsf$tagx %>% reduce(bind_rows) -> unnested_tags
unnested_tags %>% saveRDS("/datadrive/stackoverflow-current/unnested_tags.rds")

unnested_tags %>% 
  group_by(id) %>% 
  summarise(num_tags = n()) -> tut

tun <- unnested_tags %>% group_by(tagx2) %>% nest()
tun %>% saveRDS("datadrive/stackoverflow-current/tun.rds")


questions_per_tag <- tun %>% 
  mutate(num_questions = data %>% map_dbl(nrow)) %>% 
  arrange(-num_questions) %>% 
  mutate(cume = cumsum(num_questions),
         share = num_questions/sum(num_questions),
         cume_share = cume/sum(num_questions))
questions_per_tag %>% saveRDS("/datadrive/stackoverflow-current/questions_per_tag.rds")

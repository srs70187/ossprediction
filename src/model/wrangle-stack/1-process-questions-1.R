library(DBI)
library(tidyverse)
library(lubridate)
library(magrittr)
library(future)
library(future.apply)
plan(multiprocess, gc=T)
dir.create('/datadrive/stackoverflow-current',recursive=TRUE)

con <- dbConnect(RMariaDB::MariaDB(), host = "stackoverflow",
                 user = "stack", password = "overflow", dbname = "stackoverflow")

stackliteq <- dbGetQuery(con,"select Id, PostTypeId, AcceptedAnswerId, ParentId, CreationDate, DeletionDate, Score, ViewCount, OwnerUserId, OwnerDisplayName, LastEditorUserId, LastEditorDisplayName, LastEditDate, LastActivityDate, Title, Tags, AnswerCount, CommentCount, FavoriteCount, ClosedDate, CommunityOwnedDate from Posts where PostTypeId=1")

stackliteq %>% saveRDS('/datadrive/stackoverflow-current/stackliteq.rds')

stackliteq %>%
  mutate(creation_date=ymd_hms(CreationDate)) %>% 
  select(post_id = Id,
         creation_date,
         post_owner_id=OwnerUserId) -> question_info

question_info %>% saveRDS("/datadrive/stackoverflow-current/question_info.rds")

question_info %>% 
  select(post_id,
         creation_date) %>% 
  mutate(year = year(creation_date),
         month = month(creation_date),
         question_created = 1) %>% 
  select(-creation_date) -> question_info_short
question_info_short %>% saveRDS("/datadrive/stackoverflow-current/question_info_short.rds")



smart_mod <- function(sequence,mod) return(((sequence-1) %% mod)+1)

stackliteq %>% 
  select(id=Id,tags=Tags) %>% 
  ungroup %>% 
  mutate(worker = smart_mod(seq_along(row_number()),12)) %>% 
  arrange(worker) %>% 
  group_by(worker) %>% 
  nest -> qsample
qsample %>% saveRDS("/datadrive/stackoverflow-current/qsample.rds")






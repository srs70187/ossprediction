#!/bin/bash
set -euxo pipefail

echo "positional param 1 is the download parts multiple"
echo "positional param 2 is either 'github' or 'posts'"

source ${2}-download.env

mkdir -p /data/ghtorrent/download
mkdir -p /data/stack/7z/Posts
mkdir -p /data/stack/xml

python download-data.py \
    --baseurl ${stub} \
    --filename ${filename} \
    --outdir ${outdir} \
    --num-parts ${parts} \
    --num-threads ${cores} 

if [ ${2} = posts ]; then
    
    process_simple_stack() {
        cd /data/stack/7z
        rm -rf stackoverflow.com-{1}.7z
        echo "Downloading ${1} from Internet Archive"
        wget https://archive.org/download/stackexchange/stackoverflow.com-${1}.7z
        echo "Finished downloading ${1}"
        echo "Extracting ${1}"
        7z -aou e stackoverflow.com-${1}.7z -so > /data/stack/xml/${1}.xml
        echo "Finished extracting ${1}"
    }

    process_simple_stack "Votes"
    process_simple_stack "Badges"
    process_simple_stack "Tags"
    echo "downloaded Badges, Votes, and Tag Stackoverflow data and decompressed XML"

fi

chmod -R a+rwx /data


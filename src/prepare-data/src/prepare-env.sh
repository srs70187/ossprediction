#!/bin/bash
set -euo pipefail

# threads_multiple: defaults to 15. 
#   - large file downloaded in (cores*threads_multiple) parts

# cores: number of processors on the machine downloading data.

# example: 16 core machine with a thread_multiple of 15 equals 
#          240 download parts that are then recombined and decompressed

echo "Creating new ${2}-download.env file in" 
echo `pwd`

rm -rf ${2}-download.env 
touch ${2}-download.env

if [ ${2} = github ]; then
    
    threads_multiple=${1} 
    stub="http://ghtorrent-downloads.ewi.tudelft.nl/mysql/"
    
    if [ ${3} = offset ]; then 
        latest_github=$(python latest-github.py offset)
    else 
        latest_github=$(python latest-github.py)
    fi

    filename=${latest_github}.tar.gz

    outdir=/data/ghtorrent/download

elif [ ${2} = posts ]; then  

    threads_multiple=${1}
    stub="https://archive.org/download/stackexchange/"
    filename="stackoverflow.com-Posts.7z"
    outdir=/data/stack/7z/Posts

fi

cores=$(grep -c ^processor /proc/cpuinfo)
parts=$((${cores} * ${threads_multiple}))
pminus1=$((${parts}-1))
filename_prefix=`echo $filename | cut -f 1 -d '.'`

echo "cores=$cores" >> ${2}-download.env
echo "parts=$parts" >> ${2}-download.env
echo "pminus1=$pminus1" >> ${2}-download.env
echo "stub=$stub" >> ${2}-download.env
echo "filename=$filename" >> ${2}-download.env
echo "filename_prefix=$filename_prefix" >> ${2}-download.env
echo "outdir=$outdir" >> ${2}-download.env

# chmod -R a+rwx /data
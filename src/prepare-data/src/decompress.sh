#!/bin/bash
set -euxo pipefail
source ${1}-download.env

if [ ${1} = github ]; then
    cd /data/ghtorrent/download
    echo "Combining parts of github file"
    file_part_seq=($(eval echo ${filename}.{0..${pminus1}}))

    cat ${file_part_seq[@]} > ../${filename}

    cd ..
    tar -xvf ${filename} -C /data/ghtorrent --use-compress-program=pigz
    mv ${filename_prefix} mysql-github-csv
    echo "downloaded and decompressed ghtorrent data"

    echo "Splitting csvs"
    declare -a tables=(projects issues watchers commits project_commits users)

    for t in "${tables[@]}"
    do
        mkdir -p /split/$t
        split -C 256m -d /data/ghtorrent/mysql-github-csv/$t.csv /split/$t/$t.part_ &
    done
    wait

    echo "Done with ghtorrent processing"

elif [ ${1} = posts ]; then

    cd /data/stack/7z/Posts
    file_part_seq=($(eval echo ${filename}.{0..${pminus1}}))
    cat ${file_part_seq[@]} > ../${filename}
    echo "Combining parts of Stackoverflow Posts"

    7z -aou e /data/stack/7z/${filename} -o/data/stack/xml
    echo "downloaded and decompressed Stackoverflow Posts"

fi
chmod -R a+rwx /data
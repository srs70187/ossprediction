import sys
import string
from requests_html import HTMLSession

if len(sys.argv) < 2:
    offset = 1
else:
    offset = 2
        
def query_latest(offset):

    url = "http://ghtorrent.org/downloads.html"
    css = "p+ ul li:nth-child(" + str(offset) + ")"
    session = HTMLSession()
    r = session.get(url)

    latest = r.html.find(css,first=True)
    return(latest.text.split(sep=" ")[0])

#def write_to_file():
#    with open('latest-github.txt','w') as f:
#        f.write(latest.text.split(sep=" ")[0])

if __name__=='__main__':

    print(query_latest(offset))
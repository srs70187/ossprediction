#!/usr/bin/env python3 

import sys
import re
import math
import argparse
import pycurl
import pandas as pd
from requests_html import HTMLSession
from io import BytesIO
from multiprocessing.dummy import Pool as ThreadPool 

parser = argparse.ArgumentParser(description="Download data in parts from the Internet Archive")
parser.add_argument('--version', 
                    action='version', 
                    version='%(prog)s 1.0')
parser.add_argument('--baseurl', 
                    action="store", 
                    dest="stub", 
                    help="Defaults to https://archive.org/download/stackexchange/. Filename arg will be appended to this")
parser.add_argument('--filename', 
                    action="store", 
                    dest='filename', 
                    help='https://archive.org/download/stackexchange/<filename>. Note: include .7z extension', 
                    required=True)
parser.add_argument('--outdir', 
                    action="store", 
                    dest="outdir", 
                    help="full path to directory where files (x num-parts) are saved", 
                    required=True)
parser.add_argument('--num-parts',
                    action='store', 
                    dest='num_parts', 
                    type=int, 
                    help="The number of parts to divide file into", 
                    default=50)
parser.add_argument('--num-threads',
                    action='store', 
                    dest='threads', 
                    type=int, 
                    help="The number of threads to use.", 
                    default=4)

results = parser.parse_args()

session = HTMLSession()
orig_url = "%s%s" % (results.stub,results.filename)
l = session.head(orig_url)


if results.stub=="http://ghtorrent-downloads.ewi.tudelft.nl/mysql/":
    newurl = orig_url
    l2 = l
else:
    newurl = l.headers['Location']
    l2 = session.head(newurl)

print('Accept-Ranges:   ',l2.headers['Accept-Ranges'])
print('Content-Length:  ',l2.headers['Content-Length'])
print('Last Modified:   ',l2.headers['Last-Modified'])

pd.to_datetime(l2.headers['Last-Modified']) 

filesize_bytes = int(l2.headers['Content-Length'])
mm = 1000000
bb = 1000000000
filesize_mb = filesize_bytes/mm
filesize_gb = filesize_bytes/bb

print('\n\nFile is {:,.1f} MB (or {:,} Bytes)'.format(filesize_mb,filesize_bytes))

increments = math.floor(filesize_bytes/(results.num_parts))

print("\n\nWill be downloading {:} in increments of {:,.2f} MB ({:,} bytes)".format(results.num_parts,increments/mm,increments))

# def progress(download_t, download_d, upload_t, upload_d):
#     print("Total to download", download_t)
#     print("Total downloaded", download_d)
#     print("Total to upload", upload_t)
#     print("Total uploaded", upload_d)

starting_bytes = [x*increments for x in range(0,results.num_parts-1)]
ending_bytes = [ y*increments-1 for y in range(1,results.num_parts)]
byte_ranges = ["%s - %s" % (s,e) for s,e in zip(starting_bytes,ending_bytes)]

byte_ranges.append("%s - %s" % (max(ending_bytes)+1,filesize_bytes))

file_parts = ["%s/%s.%s" % (results.outdir,results.filename,x) for x in range(len(byte_ranges))]

#now list of 2 key-value dictionaries 
ranges_and_paths = [{'byte_range':b, 'file_part':f} for (b,f) in zip(byte_ranges,file_parts)]

print('\n\nstarting_bytes:      ', starting_bytes)
print('Length:  ',len(starting_bytes))

print('\n\nending_bytes:        ', ending_bytes)
print('Length:  ',len(ending_bytes))

print('\n\nbyte_ranges:         ', byte_ranges)
print('Length:  ',len(byte_ranges))

print('\n\nfile_parts:          ', file_parts )
print('Length:  ',len(file_parts))

print('\n\nranges and paths:    ', ranges_and_paths)
print('Length:  ',len(ranges_and_paths))

def curl_range(range_and_path):
    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.URL, newurl)
    #c.setopt(c.NOPROGRESS, False)
    #c.setopt(c.XFERINFOFUNCTION, progress)
    c.setopt(c.VERBOSE, True)
    c.setopt(c.RANGE, range_and_path['byte_range'])

    with open(range_and_path['file_part'], 'wb') as f: 

        c.setopt(c.WRITEDATA, buffer)
        c.perform()
        f.write(buffer.getvalue())
    
    return("Success")

pool = ThreadPool(results.threads) 
results = pool.map(curl_range, ranges_and_paths)
pool.close() 
pool.join()

print(results)

# def main():

#     try:
#         doMainthing()
#         return 0
#     except:
#         return 1
 
# if __name__ == "__main__":
#     sys.exit(main())


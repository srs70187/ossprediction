# Todo: Construcing the Build Trigger
A change in the date should trigger the beginning of this process, and the value of the date should change roughly quarterly.

> This still needs to be codified and deployed either within Jenkins, as a scheduled job on a basic vm that is always on, or as an Azure function...

```python
import pycurl
import re
from requests_html import HTMLSession
import datetime
import pandas as pd

session = HTMLSession()
l = session.head("https://archive.org/download/stackexchange/stackoverflow.com-Posts.7z")
follow = l.headers['Location']

#Need to follow that link to it's source:
l2 = session.head(follow)
print(l2.headers)
    
#   {'Server': 'nginx/1.4.6 (Ubuntu)', 'Date': 'Thu, 05 Apr 2018 23:08:02 GMT', 'Content-Type': 'application/x-7z-compressed', 'Content-Length': '12550618543', 'Last-#Modified': 'Wed, 14 Mar 2018 02:20:55 GMT', 'Connection': 'keep-alive', 'ETag': '"5aa88707-2ec133daf"', 'Expires': 'Fri, 06 Apr 2018 05:08:02 GMT', 'Cache-#Control': 'max-age=21600', 'Accept-Ranges': 'bytes'}
```


- This is the date that will change when a new file is uploaded.
    ```python
    pd.to_datetime(l2.headers['Last-Modified'])
    #   Timestamp('2018-03-14 02:20:55')
    ```

# StackExchange Data Dump Historically
These have happened on a somewhat irregular basis, although from 2016 onwards they are generally released quarterly. This doesn't detail whether it happened in the beginning or end of the month, so one month's difference can equal close to two in the edge case. 
- [Full History](https://meta.stackexchange.com/questions/224873/all-stack-exchange-data-dumps/224922)
```

Here are the torrents for each data dump release. The contents may or may not be available.

Stack Exchange Network Data Dumps
Release          BitTorrent Infohash and Download Link
2009 May         ea45080eab61ab465f647e6366f775bf25f69a61
2009 June        68d22f0f856ca5056e009ac53597a66c0cb03068
2009 July        2dca38c1c9724462ad4cedb42a1569805ab80f0c
2009 August      d348861b6d72c280f299e03fe9f9e8f3258eff19
2009 …rerelease  f6ec47fba06823e22ede500511ab7c60ceddd91d
2009 September   395bb1a89607fa05a9774bd7683eb3b6052989f7
2009 October     61b882a518f21344ca82d4632f89a1dcafcbd3ed
2009 November    107f674c9b6a481eef7c81910228bc773246035b
2009 December    11a2a48f204497f4222ff5edaf790f1d38a651fe
2010 January     5127f67fb6099d322b6467bd85a1363fe8a73c3f
2010 February    cec670fa3c54bf8403bef2d0801a6f9478fb2c68
2010 March       a790a8ab30f91c344c5b400579acd5643b2bb25c
2010 April       53adea3f9acb1972201f059604c4bdad5e3e0a8c
2010 May         f318086e65e7873d1a44de07e6d3ef4de0da0004
2010 June        2c802d003e59f84776a40b361584e1125d4eb877
2010 July        fe29a5a7278960002cae679024f0175db3e470f1
2010 August      53c3bf7f3b2ba1675acf3ac21bbaf84c63ca94c7
2010 September   8f5c7a0bac4f5ee27b7cd7a1c179ba0ba66eb847
2010 October     b549412a5e8aa70421756f0348f8063638994114
2010 November    f26b0ae2b975254676a02bdfb0fd4b7f6f78b951
2011 January     16e7c2867f7b5be3d7dbe7c3f707601b16c5f2c5
2011 April       ec636f7edc262053429653edfdf47e806f14e72f
2011 June        cdb37ea243e09fcaf54e6b7f19764765a706724c
2011 September   64d052f755e01d1c947848ad3d22180476a71a94
2011 December    77ccfd3451c442590bf5bd940362a121a16ee5a3
2011 …part 2     ccb07610f73f994b15399acdd507778dce964111
2011 …part 3     de13c16eb98bc8e95a77ed7e0ff6953d60bf90ba
2012 April       c0ecc66e8f1d630cbcd39bf523b3381a7779daa3
2012 August      ecf5ec6d0a2a3a5b582b1ef684a5fe4552ff1c8b
2012 …part 2     b06fff722ce06520b59a9db72cab9f7d008ff8c3
2013 March       47e02c81368acd9fa0f884439c00fa8439ef971f
2014 January     cc28f62bb9eb1da79ca2a782960f86f64d8ceb87
2014 May         3aed037c4c0a73d2f3eb244b99c1cd842bc88142
2014 September   b1a458cba5af9525d4012c20f9c51b35a15f0984
2015 March       0ea39049afdbaaea255ca1d0af662e2a0d503098
2016 March       57ceb5ac3e22cbf4e4de8b94f9814e809f0765bb
2016 June        e1a5e02efb60d9514c8822b1f9d3e605436caac9
2016 September   be15f50878cb77028b119a9f922f02e439481030
2016 December    fd86cb81c9ab08bd3a0969d4c700555ec7814328
2017 March       586eebe6f3c19db48a5ea471579b96d3bd3ba6e3
2017 June        fa38c0e9bbbb060927a7a7ca1f9325aa98466e1f
2017 December    e73b7025a2af72124ae49d184fa3e8cec3f66016
2018 March       4ad8edb2e0781d381d110778036703da988de71a
```

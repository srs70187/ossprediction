# Build Stages

## Prebuild
- Configure subscription values in `config/auth` 

- Make any desired changes to the default values set in `config/project-variables` and `config/resource-variables` which will control the names of Azure resources provisioned throught the build.

- From repository directory (i.e. `~/ossprediction`) run:
    ```
    make build-portal
    make connect
    ```
## Build Step 1
### Create Azure Resources and Source, Stage Input Data

This set of commands will:
- Create a new resource group
- Launch a Standard_DS14_v2 virtual machine (16 cores and 55GB RAM)
    - [`config/resource-variables.mk`](config/resource-variables.mk)
- Install apt libraries required for the build 
    - [`lib/install.sh`](lib/install.sh))
- Configure network rules to open ports for inbound TCP, databases, Rstudio server, etc. 
    - [`networking.mk`](build/networking.mk)
- Create, attach, and format a "raw data" disk, and a "model data" hard disk which will be the source for storing all build artifacts
    - [`disks.mk`](build/disks.mk)
    - [`lib/format_mount_disk.sh`](lib/format_mount_disk.sh)
- Download data from GHTorrent Project, extract it to disk, and split files into collections of 256MB CSVs which will be used later during modeling.
    > Code located in `src/prepare-data` directory
    - [`download-data.py`](src/prepare-data/src/download-data.py)
    - [`decompress.sh`](src/prepare-data/src/decompress.sh)
    - [`prepare-env.sh`](src/prepare-data/src/prepare-env.sh)
    - [`docker-compose.yml`](src/prepare-data/docker-compose.yml) for orchestrating
- Download StackOverflow data from the internet archive, extract it, and then load it into a MYSQL database.
    > In addition to the services described for GitHub download and extraction, see `src/stack-db` directory.

```
make resource-group \
    && make new-vm \
    && make configure-vm \
    && make network \
    && make disks \
    && make github \
    && make stack \
    && make stack-initdb \
    && make load-tags \
    && make load-badges \
    && make load-votes \
    && make load-posts
```


## Build Step 1B (for first builds only)
Specifically for `VS Telemetry - Staging` configuration.

> If intending to configure a subscription other than the one Filip provided to Steven, it is necessary to alter the if-statement in [`config/sp-login.sh`](config/sp-login.sh) if the intention is to use this subscription to persist resources. 

> Note also persisting resources is not required to complete the build, rather the intention is to minimize exposure to inevitable changes in the many dependencies that this build process relies on. 

### Create R Environment
- This will build the R Studio Server and Shiny Server environment as a docker image with all necessary packages preloaded.
    > See `src/model/rinstance` directory
    - [`model.mk`](build/model.mk)
    - [`packages.R`](src/model/rinstance/packages.R)
    - [`docker-compose.yml`](src/model/docker-compose.yml)
- The docker image is persisted as a `.tar` file and then uploaded as a blob to the ossdatastore resource group in Azure. This image should be used for all successive builds where the model code has not changed. Commands to download and load the docker image are used instead of these commands going forward.

```
make rsync \
    && make build-rinstance \
    && make upload-rinstance

```

### Provision for dynamic coverage of StackOverflow badges

- Initialize record of scrape-history dates and sync with azure storage container

```
make permissions-open \
    && create-scrape-history-stor \
    && init-scrape-history \
    && scrape-history-upload

```

## Build Part 2 

### Increase VM RAM and stand up R Environment for Modeling and Data Staging

```
make resize-vm-large \
    && make standard-disk-remount \
    && make model-environment
```

### Badge Scrape
```
make permissions-open \
    && make badge-df-download \
    && make scrape-history-download \
    && make wrangle-stack-4 \
    && make wrangle-stack-5 \
    && make wrangle-stack-6 \
    && make badge-df-upload \
    && make scrape-history-upload
```
### Process Badges and Internet Archive data
Creates panel dataset of stack metrics by tag-month.

```
make set-latest-date \
    && make wrangle-stack-1 \
    && make wrangle-stack-2 \
    && make wrangle-stack-3 \
    && make wrangle-stack-7 \
    && make wrangle-stack-8
```

### Run ML Model Code

```
make set-latest-date \
    && make ml-model-1 \
    && make ml-model-2 \
    && make ml-model-3 \
    && make ml-model-4 \
    && make ml-model-5 \
    && make ml-model-6 \
    && make ml-model-7 \
    && make ml-model-8 \
    && make ml-model-9 \
    && make ml-model-10 \
    && make ml-model-11 \
    && make ml-model-12 \
    && make ml-model-13 \
    && make ml-model-14 \
    && make ml-model-15 \
    && make ml-model-16 \
    && make ml-model-17

```

## Snapshot Build Disks
```
make save-raw-data-disk \
    && make save-model-data-disk

```

## Notes
- Shiny App
- GHTorrent Extract Offset
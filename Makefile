include config/project-variables.mk
include config/resource-variables.mk
include config/interactive.mk
include build/disks.mk
include build/networking.mk
include build/vm-provisional.mk
include build/source-data.mk
include build/badges.mk
include build/model.mk

resource-group :
	az group create -n $(resource_group) -l $(location)

new-vm :
	az vm create \
		-n $(vm_name) \
		-g $(resource_group) \
		--size $(vm_sku) \
		--image UbuntuLTS \
		--os-disk-name $(os_disk_name) \
		--os-disk-size-gb 150 \
		--storage-sku Premium_LRS \
		--admin-username $(vm_admin) \
		--authentication-type ssh \
		--ssh-key-value @$(ssh_directory_location)/.ssh/$(ssh_key_prefix).pub

vm_ip = $$(az vm list-ip-addresses -n $(vm_name) -g $(resource_group) | jq -r ' .[].virtualMachine.network.publicIpAddresses[].ipAddress ')
userathost := $(vm_admin)@$(vm_ip)

query-ip :
	@echo $(vm_ip)

ssh-to-vm := ssh -o "ServerAliveInterval=30" -o "StrictHostKeyChecking=no" $(userathost)
ssh-command := ssh -o "ServerAliveInterval=30" -o "StrictHostKeyChecking=no" $(userathost)
ssh-command-t := ssh -o "ServerAliveInterval=30" -o "StrictHostKeyChecking=no" -t $(userathost)

ssh-to-remote :
	$(ssh-to-vm) 

rsync :
	$@ -avh -e "ssh -o StrictHostKeyChecking=no" /$(build_repo) $(userathost):/home/$(vm_admin)

configure-vm : rsync 
	$(ssh-command) 'cp ossprediction/config/.ssh/* ~/.ssh/ '
	$(ssh-command) 'bash -s' < lib/install.sh #installs docker, compose, packages, etc on remote os

network : open-db-ports open-modeling-ports open-dbviewer-ports inbound-tcp

disks : new-disks attach-disks format-disks 

purge-build :
	az group delete -g $(resource_group) -y

remote-htop :
	$(ssh-command-t) "htop"
#downlaod the processed files

# OSS Predictions Repository

This code repository is designed to serve as an end-to-end incremental update and refresh of the data and subsequent forecasts for the various predictive-models designed to enhance awareness of trends in Open Source Software. The following documentation is intended to serve primarily as a set of instructions for the build process that is required for each incremental update. Though to the extent possible, additional detail is provided on the components of the build process with the intention of allowing for the future maintainers of this repository to troubleshoot and/or make updates to the existing codebase. 

## Overview
At a high level the "build-process" is a set of instructions that will utilize a combination of Azure Services provisioned by the Azure CLI in order to:
- Download data from the [Stackoverflow Data Dump hosted on the Internet Archive](https://archive.org/details/stackexchange)
- Extract StackOverflow archive and initialize a MySQL database containing the data
- Download data from [the GHTorrent Project's MySQL extracts](http://ghtorrent.org/downloads.html)
- Extract the GHTorrent Archive and split the individual csv into consumable 256MB Pieces.
- Source and process the latest repository metadata from the GitHub API
- Source and process [Notable](https://stackoverflow.com/help/badges/27) and [Famous](https://stackoverflow.com/help/badges/28) Badge data directly from StackOverflow's html.
- Refit models given the updates to the data sources mentioned, and generate predictions indexed for the upcoming 18 months based on the refit.

# Pre-Build Configuration

At a minimum the "host machine" or the machine intended to run the build needs to have Docker installed. While not required, it is suggested that the host machine is running Ubuntu Linux  

1. Has Docker (CE) installed.
1. 
It is necessary to provide details for one's Azure subscription in order to run almost every aspect of the build. This section outlines the steps required for Azure Subscription Configuration.

## Authentication
In order for resources to be provision with a new subscription, an Azure AD app with a Service Principal must be created. This is best done on the Azure Portal [following these directions](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-create-service-principal-portal). 

See instructions below regarding which information from this process needs to be copied locally.

## Service Principal Credentials
1. The value of the app key password is shown only once in this step. Please save its value to be used in the next step. 
![alt text](docs/.sp-key.png)

1. Assign the role of the SP to `Contributor` or `Owner`. `Owner` is suggested. 

1. Once the SP is configured, copy or export its `Manifest` file locally. If from the main page, do `Azure Active Directory blade >> App registrations blade >> follow link to new app >> toggle Manifest`. 
![alt text](docs/.app-manifest.png)

1. navigate to the `config/auth` directory and replace the value of the `.spkey` file with the app key password
1. Save the value of the app key password to the files `


# Build Stages

## Prebuild
- Configure subscription values in `config/auth` 

- Make any desired changes to the default values set in `config/project-variables` and `config/resource-variables` which will control the names of Azure resources provisioned throught the build.

- From repository directory (i.e. `~/ossprediction`) run:
    ```
    make build-portal
    make connect
    ```
## Build Step 1
### Create Azure Resources and Source, Stage Input Data

This set of commands will:
- Create a new resource group
- Launch a Standard_DS14_v2 virtual machine (16 cores and 55GB RAM)
    - [`config/resource-variables.mk`](config/resource-variables.mk)
- Install apt libraries required for the build 
    - [`lib/install.sh`](lib/install.sh))
- Configure network rules to open ports for inbound TCP, databases, Rstudio server, etc. 
    - [`networking.mk`](build/networking.mk)
- Create, attach, and format a "raw data" disk, and a "model data" hard disk which will be the source for storing all build artifacts
    - [`disks.mk`](build/disks.mk)
    - [`lib/format_mount_disk.sh`](lib/format_mount_disk.sh)
- Download data from GHTorrent Project, extract it to disk, and split files into collections of 256MB CSVs which will be used later during modeling.
    > Code located in `src/prepare-data` directory
    - [`download-data.py`](src/prepare-data/src/download-data.py)
    - [`decompress.sh`](src/prepare-data/src/decompress.sh)
    - [`prepare-env.sh`](src/prepare-data/src/prepare-env.sh)
    - [`docker-compose.yml`](src/prepare-data/docker-compose.yml) for orchestrating
- Download StackOverflow data from the internet archive, extract it, and then load it into a MYSQL database.
    > In addition to the services described for GitHub download and extraction, see `src/stack-db` directory.

```
make resource-group \
    && make new-vm \
    && make configure-vm \
    && make network \
    && make disks
*
make github \
    && make stack \
    && make stack-initdb \
    && make load-tags \
    && make load-badges \
    && make load-votes \
    && make load-posts
```

*: Due to a clean up tool running on our Azure subscription the network setup can sometimes result in an inability to SSH into the new VM.  The 'make network' command will attempt to open the SSH port 22 and put a lock on the network security group to prevent clean up tool removing it but the is potential for timing issues where clean up tool will remove the port before the lock is set.  Another issue seen recently is that the VM subNet gets attached to a clean up tool created network security group, in this case you'll have to manualy switch it to point to the tool created one (ossm-vmNSG).  Long run we need to figure out a security rule to allow one VM to SSH to another without violating network rules that the clean up tool will remove.

## Build Step 1B (for first builds only)
Specifically for `VS Telemetry - Staging` configuration.

> If intending to configure a subscription other than the one Filip provided to Steven, it is necessary to alter the if-statement in [`config/sp-login.sh`](config/sp-login.sh) if the intention is to use this subscription to persist resources. 

> Note also persisting resources is not required to complete the build, rather the intention is to minimize exposure to inevitable changes in the many dependencies that this build process relies on. 

### Create R Environment
- This will build the R Studio Server and Shiny Server environment as a docker image with all necessary packages preloaded.
    > See `src/model/rinstance` directory
    - [`model.mk`](build/model.mk)
    - [`packages.R`](src/model/rinstance/packages.R)
    - [`docker-compose.yml`](src/model/docker-compose.yml)
- The docker image is persisted as a `.tar` file and then uploaded as a blob to the ossdatastore resource group in Azure. This image should be used for all successive builds where the model code has not changed. Commands to download and load the docker image are used instead of these commands going forward.

```
make rsync \
    && make build-rinstance \
    && make upload-rinstance

```

### Provision for dynamic coverage of StackOverflow badges

- Initialize record of scrape-history dates and sync with azure storage container

```
make permissions-open \
    && make create-scrape-history-stor \
    && make init-scrape-history \
    && make scrape-history-upload

```

## Build Part 2 

### Increase VM RAM and stand up R Environment for Modeling and Data Staging

```
make resize-vm-large \
    && make standard-disk-remount \
    && make model-environment
```

### Badge Scrape
```
make permissions-open \
    && make badge-df-download \
    && make scrape-history-download \
    && make wrangle-stack-4 \
    && make wrangle-stack-5 \
    && make wrangle-stack-6 \
    && make badge-df-upload \
    && make scrape-history-upload
```
### Process Badges and Internet Archive data
Creates panel dataset of stack metrics by tag-month.

```
make set-latest-date \
    && make wrangle-stack-1 \
    && make wrangle-stack-2 \
    && make wrangle-stack-3 \
    && make wrangle-stack-7 \
    && make wrangle-stack-8
```

### Run ML Model Code

```
make set-latest-date \
    && make ml-model-1 \
    && make ml-model-2 \
    && make ml-model-3 \
    && make ml-model-4 \
    && make ml-model-5 \
    && make ml-model-6 \
    && make ml-model-7 \
    && make ml-model-8 \
    && make ml-model-9 \
    && make ml-model-10 \
    && make ml-model-11 \
    && make ml-model-12 \
    && make ml-model-13 \
    && make ml-model-14 \
    && make ml-model-15 \
    && make ml-model-16 \
    && make ml-model-17

```

## Snapshot Build Disks
```
make save-raw-data-disk \
    && make save-model-data-disk

```
## Auxillary 

While unlikely, it is inevitable that at some point the VM will go offline either during or between build steps. If that happens you will get an error with something along the lines of not being able to read or write to a file located on `raw-data-disk` or `datadrive` depending on where you are in the build. If you see an error like this, please run the following before trying to pick up where the build threw the error.

### If VM reboots during Build Step 1
```
make network \
    && make standard-disk-remount \
    && make rsync \
    && make permissions-open 

```
### If VM reboots during Build Step 2
```
make network \
    && make standard-disk-remount \
    && make rsync \
    && make permissions-open \
    && make model-environment
```



## Notes
- Shiny App
- GHTorrent Extract Offset

# Misc.

- You can always override commands with the `-e <variable>=<value>` switch at the command line. For example, to snapshot the model data disk in a resource group other than the one defined in `project-variables.mk`, you can do
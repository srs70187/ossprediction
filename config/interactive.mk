
.PHONY: build connect reconnect

build-portal :
	docker build --no-cache -t $(build_name):$(build_version) config

connect :
	docker run --net=host \
		--rm \
    	-v $(build_repo_dir)/config/.ssh:/root/.ssh \
    	-v $(build_repo_dir):/$(build_repo) \
    	-v $(modeling_repo_dir):/$(modeling_repo) \
		-w /$(build_repo) \
		-it $(build_name):$(build_version) bash


halt :
	docker stop $$(docker ps -aq)

purge :
	docker system prune --force
	docker rmi $$(docker images -q) --force
	docker volume prune --force
	docker system prune -a --force

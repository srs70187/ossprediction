#!/bin/bash

service_principal=$(cat /config/auth/sp-manifest.json | jq -r '.appId')
password=$(cat /config/auth/.spkey)
tenant=$(cat /config/auth/.tenant)

az login --service-principal \
    -u ${service_principal} \
    -p ${password} \
    -t ${tenant}

export stor055=$(az storage account list --output json | jq -r '.[] | select(.name=="datastore055") | .name')
export stor056=$(az storage account list --output json | jq -r '.[] | select(.name=="datastore056") | .name')

if [ "$stor055" == "datastore055" ]; then
    echo "datastore055" > ~/stor_name
else 
    echo "datastore056" > ~/stor_name
fi

#!/bin/bash

service_principal=$(cat ~/ossprediction/config/auth/sp-manifest.json | jq -r '.appId')
password=$(cat ~/ossprediction/config/auth/.spkey)
tenant=$(cat ~/ossprediction/config/auth/.tenant)

az login --service-principal \
    -u ${service_principal} \
    -p ${password} \
    -t ${tenant}

export subName=$(az account list --query [].name --output tsv)
if [ $subName = msft-oss-azure-services ]; then
    echo "datastore055" > ~/stor_name
else 
    echo "datastore056" > ~/stor_name
fi
SHELL=/bin/bash
epoch_timestamp = $$(date "+%s")
year = $$(date +%Y) 
month = $$(date +%m)
day = $$(date +%Y-%d) 
year_month ?= $$(date +%Y-%m)
year_month_day = $$(date +%Y-%m-%d)

build_name = cliportal
build_version = 2

build_repo = ossprediction
build_repo_dir = /home/$${USER}/$(build_repo)

modeling_repo = ossproject
modeling_repo_dir = /home/$${USER}/$(modeling_repo)

ssh_directory_location = /$(build_repo)/config
ssh_directory_name = .ssh
ssh_key_prefix ?= id_rsa

# vmss_build_name=vmssportal
# vmss_build_version=1.0
# vmss_workdir=/msft-oss-eng/data/stackoverflow-crawler

timestamp :
	@echo $(timestamp)
vars :
	@echo Defined project level variables


resource_prefix=ossm
resource_group = $(resource_prefix)-build
location = westus

vm_name = $(resource_prefix)-vm
vm_admin=ossadmin
vm_sku=Standard_DS5_v2
vm_sku_small = Standard_DS3_v2
vm_sku_large = Standard_DS14_v2

os_disk_name = $(resource_prefix)-osdisk

data_disk_name = raw-data
$(data_disk_name) = /dev/sdc
data_disk_size = 1024

model_disk_name = model-data
$(model_disk_name) = /dev/sdd
model_disk_size = 1024

stor_rg := ossdatastore
stor_location := westus
storage_account_cstring := $$(az storage account show-connection-string --name $$(cat ~/stor_name) -g $(stor_rg) --output json | jq '.connectionString')

recall-disks :
	@echo $(data_disk_name)-disk: $($(data_disk_name))
	@echo $(model_disk_name)-disk: $($(model_disk_name))




#!/bin/bash
set -euxo pipefail

(echo n; echo p; echo 1; echo; echo; echo w) | sudo fdisk ${2}
sudo mkfs -t ext4 ${2}1

sudo rm -rf /${1}
sudo mkdir -p /${1}
sudo mount ${2}1 /${1}
sudo chmod -R a+rwx /${1}

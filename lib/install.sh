#!/bin/bash
set -euxo pipefail

install_apt_libraries() {
    declare -a libraries=(jq git pigz htop p7zip-full make pv linux-headers-$(uname -r) build-essential dkms)
    sudo apt-get update && sudo apt-get dist-upgrade -y
    for p in "${libraries[@]}" 
    do
    sudo apt-get install $p -y
    done
}

install_docker() {
    curl -fsSL get.docker.com -o get-docker.sh
    sudo sh get-docker.sh

    sudo usermod -aG docker $USER

    rm get-docker.sh

    sudo su - $USER
}

install_docker_compose() {
    
    compose_version=${1:-1.21.0}

    sudo curl -L https://github.com/docker/compose/releases/download/${compose_version}/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose

    sudo curl -L https://raw.githubusercontent.com/docker/compose/${compose_version}/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose

}


configure_git()
{
    git_user_name=${1:-'srs70187'}
    git_user_email=${2:-'steven.r.soloway@gmail.com'}    

    git config --local user.name $git_user_name
    git config --local user.email $git_user_email

}

install_azcli() {
    export AZ_REPO=$(lsb_release -cs)
    echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | \
        sudo tee /etc/apt/sources.list.d/azure-cli.list

    sudo apt-key adv --keyserver packages.microsoft.com --recv-keys 52E16F86FEE04B979B07E28DB02C46DF417A0893 && sudo apt-get update 
    sudo apt-get install apt-transport-https -y
    sudo apt-get update && sudo apt-get install azure-cli -y

}

authenticate_azcli() {
    service_principal=$(cat ~/ossprediction/config/auth/sp-manifest.json | jq -r '.appId')
    password=$(cat ~/ossprediction/config/auth/.spkey)
    tenant=$(cat ~/ossprediction/config/auth/.tenant)

    az login --service-principal \
        -u ${service_principal} \
        -p ${password} \
        -t ${tenant}
}

if which pigz > /dev/null; then echo apt libraries already installed; else install_apt_libraries; fi
if which docker-compose > /dev/null; then echo Docker Compose already installed; else install_docker_compose; fi
if which az > /dev/null; then echo apt libraries already installed; else install_azcli; fi
if [ -z $(az account list --query [].id --output tsv) ] > /dev/null; then authenticate_azcli; else echo "already logged into azure cli"; fi
if which docker > /dev/null; then echo Docker already installed; else install_docker; fi

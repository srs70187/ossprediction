#!/bin/bash
declare -a tables=(projects issues watchers commits project_commits users)

for t in "${tables[@]}"
do
    mkdir -p /datadrive/github-tables/$t
    split -C 256m -d /raw-data-disk/ghtorrent/mysql-github-csv/$t.csv /datadrive/github-tables/$t/$t.part_ &
done


stitch-vm : 
	az vm create \
		-n $(vm_name) \
		-g $(resource_group) \
		--size $(vm_sku) \
		--os-type linux \
		--attach-os-disk $(os_disk_name)
		
delete-vm : 
	az vm delete -n $(vm_name) -g $(resource_group) -y

resize-vm-small :
	az vm resize -g $(resource_group) -n $(vm_name) --size $(vm_sku_small)

resize-vm-large :
	az vm resize -g $(resource_group) -n $(vm_name) --size $(vm_sku_large)	

resize-vm :
	az vm resize -g $(resource_group) -n $(vm_name) --size $(vm_sku)


last-build-disks :
	az disk create \
		-n raw-data-2018-04 \
		-g $(resource_group) \
		--size-gb 1024 \
		--sku Premium_LRS \
		--source $$(az snapshot show -n raw-data-2018-05 -g ossdisks | jq -r '.id')

	az vm disk attach \
		--disk raw-data-2018-04 \
		-g $(resource_group) \
		--vm-name $(vm_name)

	az disk create \
		-n model-data-2018-04 \
		-g $(resource_group) \
		--size-gb 1024 \
		--sku Premium_LRS \
		--source $$(az snapshot show -n last-model-disk-2018-04 -g ossdisks | jq -r '.id')

	az vm disk attach \
		--disk model-data-2018-04 \
		-g $(resource_group) \
		--vm-name $(vm_name)

last-build : resize-vm-large last-build-disks standard-disk-remount download-rinstance model-environment
#last-build : resource-group new-vm rsync configure-vm network resize-vm-large last-build-disks standard-disk-remount download-rinstance model-environment

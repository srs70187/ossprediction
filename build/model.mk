azure-login: 
	$(ssh-command) "~/ossprediction/config/sp-login.sh"

permissions-open :
	$(ssh-command) "sudo chmod -R a+rwx /raw-data-disk"
	$(ssh-command) "sudo chmod -R a+rwx /datadrive"

build-rinstance :
	$(ssh-command) "cd ~/ossprediction/src/model/rinstance; docker build -t rinstance .; docker save -o ~/rinstance.tar rinstance"

upload-rinstance :
	az storage container create \
	--name docker \
	--connection-string $(storage_account_cstring)
	$(ssh-command) "az storage blob upload --container-name docker --connection-string $(storage_account_cstring) --file ~/rinstance.tar --name rinstance.tar"


download-rinstance :
	$(ssh-command) "az storage blob download --container-name docker --connection-string $(storage_account_cstring) --file ~/rinstance.tar --name rinstance.tar"
	$(ssh-command) "docker load -i ~/rinstance.tar"  

github-api-dataset : permissions-open ml-model-1

model-environment :
	$(ssh-command) "cd ~/ossprediction/src/model && docker-compose down"
	sleep 10
	$(ssh-command) "cd ~/ossprediction/src/model && docker-compose up -d --build"

set-latest-date :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/wrangle-stack/latest-stack-date.R"

wrangle-stack-1 : permissions-open
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/wrangle-stack/1-process-questions-1.R"

wrangle-stack-2 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/wrangle-stack/2-process-questions-2.R"

wrangle-stack-3 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/wrangle-stack/3-process-votes-1.R"

wrangle-stack-4 : permissions-open
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/wrangle-stack/4-process-badges-1.R"

wrangle-stack-5 : 
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/wrangle-stack/5-process-badges-2.R"

wrangle-stack-6 : 
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/wrangle-stack/6-process-badges-3.R"

wrangle-stack-7 : 
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/wrangle-stack/7-process-badges-4.R"

wrangle-stack-8 : 
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/wrangle-stack/8-combine-all-1.R"



ml-model-1 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/1-get-top-gh-repos.R"
ml-model-2 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/2-get-so-features.R"

ml-model-3 : permissions-open
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/3-stage-github-1.R"
ml-model-4 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/4-stage-github-2.R"
ml-model-5 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/5-stage-github-3.R"
ml-model-6 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/6-github-stage-4.R"
ml-model-7 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/7-committer-network-1.R"
ml-model-8 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/8-committer-network-2.R"
ml-model-9 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/9-committer-network-3.R"
ml-model-10 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/10-repo-info-aux.R"
ml-model-11 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/11-committer-network-features-1.R"
ml-model-12 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/12-contributor-stats.R"
ml-model-13 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/13-prepare-features-1.R"
ml-model-14 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/14-prepare-features-2.R"
ml-model-15 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/15-prepare-features-3.R"
ml-model-16 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/16-prepare-features-4.R"
ml-model-17 :
	$(ssh-command) "docker exec rinstance Rscript /ossprediction/src/model/ml/17-run-models-1.R"


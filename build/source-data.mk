github-dl : rsync
	$(ssh-command) "cd ~/ossprediction/src/prepare-data && docker-compose up --build download-ghtorrent"

github-csv : rsync
	$(ssh-command) "cd ~/ossprediction/src/prepare-data && docker-compose up --build decompress-ghtorrent"

github : github-dl github-csv

stack-dl : rsync
	$(ssh-command) "cd ~/ossprediction/src/prepare-data && docker-compose up --build download-stack"

stack-xml : rsync
	$(ssh-command) "cd ~/ossprediction/src/prepare-data && docker-compose up --build decompress-stack"

stack : stack-dl stack-xml

stack-initdb : stack-db-down rsync
	sleep 15
	$(ssh-command) sudo rm -rf /datadrive/stackdb/*
	sleep 5
	$(ssh-command) "sudo chmod go-wx ~/ossprediction/src/stack-db/conf.d/my.cnf; cd ~/ossprediction/src/stack-db && docker-compose up -d --build initdb"

stack-db-down :
	$(ssh-command) "cd ~/ossprediction/src/stack-db && docker-compose down"

load-% :
	$(ssh-command) "docker exec initdb bash /run/load-tables.sh $*"

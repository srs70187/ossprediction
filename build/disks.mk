new-disks : new-raw-data-disk new-model-data-disk
attach-disks : attach-raw-data-disk attach-model-data-disk
format-disks : format-raw-data-disk format-model-data-disk

create-datastore :
	az group create -n ossdatastore


import-%-disk :
	az disk create \
		-n  $* \
		-g $(resource_group) \
		--size-gb 1024 \
		--sku Premium_LRS \
		--source $$(az snapshot show -n $* -g ossdatastore | jq -r '.id')

snapshot-%-disk :
	az snapshot create \
		--resource-group datastore \
		--name $*-$(epoch_timestamp) \
		--source $$(az disk show -n $* -g $(resource_group) | jq -r '.id')

save-%-disk :
	az snapshot create \
		--resource-group $(stor_rg) \
		--name $*-$(year_month) \
		--source $$(az disk show -n $* -g $(resource_group) | jq -r '.id')

save-raw-data-disk :
	az snapshot create \
		--resource-group $(stor_rg) \
		--name raw-data-$(year_month) \
		--source $$(az disk show -n raw-data -g $(resource_group) | jq -r '.id')

freeze-model-data-disk :
	az snapshot create \
		--resource-group $(stor_rg) \
		--name model-data-half-$(year_month) \
		--source $$(az disk show -n model-data -g $(resource_group) | jq -r '.id')

save-model-data-disk :
	az snapshot create \
		--resource-group $(stor_rg) \
		--name model-data-final-$(year_month) \
		--source $$(az disk show -n model-data -g $(resource_group) | jq -r '.id')


new-%-disk :
	az disk create \
		-n  $* \
		-g $(resource_group) \
		--size-gb 1024 \
		--sku Premium_LRS 

attach-%-disk : 
	az vm disk attach \
		--disk $* \
		-g $(resource_group) \
		--vm-name $(vm_name) \
		--caching ReadWrite

format-raw-data-disk : 
	$(ssh-command) 'bash -s' < lib/format_mount_disk.sh $(data_disk_name)-disk $($(data_disk_name))

format-model-data-disk : 
	$(ssh-command) 'bash -s' < lib/format_mount_disk.sh datadrive $($(model_disk_name))
	$(ssh-command) "sudo mkdir -p /datadrive/peter; sudo mkdir -p /datadrive/stackoverflow-current"

delete-vm-osdisk :
	az disk delete -y \
		--resource-group $(resource_group) \
		--name $(os_disk_name)

standard-disk-remount :
	$(ssh-command) "sudo umount /dev/sdc1; sudo mkdir -p /raw-data-disk; sudo mount /dev/sdc1 /raw-data-disk; sudo chmod -R a+rwx /raw-data-disk"
	$(ssh-command) "sudo umount /dev/sdd1; sudo mkdir -p /datadrive; sudo mount /dev/sdd1 /datadrive; sudo chmod -R a+rwx /datadrive"


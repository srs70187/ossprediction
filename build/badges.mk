create-scrape-history-stor :
	az storage container create \
	--name scrape-history \
	--connection-string $(storage_account_cstring)

init-scrape-history :
	$(ssh-command) "mkdir -p /datadrive/badges; cp -R ~/ossprediction/config/scrape-history /datadrive/badges"

badge-df-download :
	$(ssh-command) "mkdir -p /datadrive/badges/processed"
	$(ssh-command) "az storage blob download-batch --source processed --destination /datadrive/badges/processed --connection-string $(storage_account_cstring)"

scrape-history-download :
	$(ssh-command) "mkdir -p /datadrive/badges/scrape-history"
	$(ssh-command) "az storage blob download-batch --source scrape-history --destination /datadrive/badges/scrape-history --connection-string $(storage_account_cstring)"

badge-df-upload :
	$(ssh-command) "az storage blob upload-batch --source /datadrive/badges/processed --destination processed --connection-string $(storage_account_cstring)"

scrape-history-upload :
	$(ssh-command) "az storage blob upload-batch --source /datadrive/badges/scrape-history --destination scrape-history --connection-string $(storage_account_cstring)"

marginal-badge-scrape : wrangle-stack-4 wrangle-stack-5 wrangle-stack-6 badge-df-upload scrape-history-upload
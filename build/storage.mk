new-stor-setup :
	az group create \
		--name ${stor_rg} \
		--location ${stor_location} > stor-rg.json

	az storage account create \
		--name ${stor_name} \
		--resource-group ${stor_rg} \
		--sku Standard_LRS > stor-account.json

	az storage account keys list \
		-n ${stor_name} \
		-g ${stor_rg} > stor-keys.json
	
	az storage container create \
		--name famous \
		--connection-string $(storage_account_cstring)
	
	az storage container create \
	--name notable \
	--connection-string $(storage_account_cstring)

	az storage container create \
	--name processed \
	--connection-string $(storage_account_cstring)

	az storage container create \
	--name docker \
	--connection-string $(storage_account_cstring)


	
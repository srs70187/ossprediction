open-db-ports : 
	az vm open-port -n $(vm_name) \
		--port 3306 \
		-g $(resource_group) \
		--priority 801 
	az vm open-port -n $(vm_name) \
		--port 3307 \
		-g $(resource_group) \
		--priority 802 
open-dbviewer-ports : 
	az vm open-port -n $(vm_name) \
		--port 8080 \
		-g $(resource_group) \
		--priority 803 
	az vm open-port -n $(vm_name) \
		--port 8081 \
		-g $(resource_group) \
		--priority 804 
		
open-modeling-ports :
	az vm open-port -n $(vm_name) \
		--port 8790 \
		-g $(resource_group) \
		--priority 805
	az vm open-port -n $(vm_name) \
		--port 3636 \
		-g $(resource_group) \
		--priority 806

inbound-tcp :
	az network nsg rule create \
		--name allowSSH-ossbuild \
		--nsg-name $(resource_prefix)-vmNSG \
		--priority 100 \
		--access Allow \
		--resource-group $(resource_group) \
		--description "Open inbound tcp on port 22 for model build vm" \
		--direction Inbound \
		--protocol Tcp \
		--destination-port-range 22 \
		--source-address-prefixes \* \
		--source-port-ranges *

nsg-lock :
	az lock create -t ReadOnly \
		-n block \
		--resource /subscriptions/a9e93d76-e401-4966-98f4-c838883a468c/resourceGroups/$(resource_group)/providers/Microsoft.Network/networkSecurityGroups/$(resource_prefix)-vmNSG


ssh-tunnel :
	ssh -f -N -T -R 20000:localhost:22 $(userathost)
